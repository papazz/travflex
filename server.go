package main

import (
	"bitbucket.org/papazz/travflex/app"
	"bitbucket.org/papazz/travflex/logger"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/justinas/alice"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/unrolled/render"
	"log"
	"net/http"
)

func main() {
	env := &app.Env{
		Render: render.New(),
		HTTPClient: &http.Client{
			Timeout: app.GetTimeout(),
		},
	}

	stdChain := alice.New(logger.LoggingHandler(env))

	r := mux.NewRouter()
	r.Handle("/book", app.Instrument("book", stdChain.Then(app.Handler{E: env, H: app.BookHotel}))).Methods("POST")
	r.Handle("/cancel", app.Instrument("cancel", stdChain.Then(app.Handler{E: env, H: app.CancelPolicy}))).Methods("POST")
	r.Handle("/additional_info", app.Instrument("additional_info", stdChain.Then(app.Handler{E: env, H: app.CancelReserv}))).Methods("POST")
	r.Handle("/metrics", prometheus.Handler())
	r.Handle("/search", app.Instrument("search", stdChain.Then(app.Handler{E: env, H: app.SearchHotel}))).Methods("POST")
	r.HandleFunc("/healthcheck", func(w http.ResponseWriter, r *http.Request) { fmt.Fprintf(w, "OK") })
	http.Handle("/", r)
	log.Println("TRAVFLEX server starting...")
	log.Fatal(http.ListenAndServe(":3002", nil))
}
