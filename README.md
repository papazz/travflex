# Partner - TRAVFLEX 

> __Note__: asterisk `*` staying beside of param. name in description will denote param. as __required__

## API document
API_Specs/Travflex

## Search
### API search request
```json
{
    "params": {
        "check_in_date": "2016-05-01",
        "check_out_date": "2016-05-05",
        "room_count": 2,
        "adult_count": 4,
        "child_count": 2,
        "partner_destination_ids": [<country_code>, <city_code>]
    },
    "credentials": {
        "partner_id": "<partner_id>",
        "pax_passport": "<pax_passport_code>",
        "username": "<username>",
        "password": "<password>", 
        "url": "http://xml.travflex.com/11WS_SP2_1/ServicePHP/SearchHotel.php"
    }
}
```

* params.check_in_date* string with format `YYYY-MM-DD`
* params.check_out_date string with format `YYYY-MM-DD`
* params.room_count* number of room `(min: 1; max: 10)`, type `Int`
* params.adult_count* number of adult Person `(min: 1; max: 4*room_count)`, type `Int`
* params.child_count number of children `(min: 0; max: 2*room_count)`, type `Int`
* params.partner_destination_ids* array of `String` of 2 elements where first and second ones correspondly 
    * Country code
    * City code
* credentials.partner_id* is a username provided by supplier, type `String`
* credentials.pax_passport* guest passport code, type `String`
* credentials.username* username provided by supplier, type `String`
* credentials.password* password, type `String`
* credentials.url* supplier url for hotel search, type `String`

> Note: that each extra hotel_id originates extra query and after 
> they aggregates async answers to one response

### TRAVFLEX search request
```xml
<Service_SearchHotel>
    <AgentLogin>
        <AgentId>partnerId</AgentId>
        <LoginName>login</LoginName>
        <Password>password</Password>
    </AgentLogin>
    <SearchHotel_Request>
        <PaxPassport>paxPassport</PaxPassport>
        <DestCountry>destinationCountryCode</DestCountry>
        <DestCity>destinationCityCode</DestCity>
        <Period checkIn="2016-05-12" checkOut="2016-05-13"/>
        <RoomInfo>
            <AdultNum RQBedChild="N" RoomType="Twin">2</AdultNum>
            <ChildAges>
                <ChildAge>8</ChildAge>
            </ChildAges>
            <ChildAges>
                <ChildAge>8</ChildAge>
            </ChildAges>
        </RoomInfo>
        <RoomInfo>
            <AdultNum RQBedChild="N" RoomType="Twin">2</AdultNum>
        </RoomInfo>
    </SearchHotel_Request>
</Service_SearchHotel>
```

### TRAVFLEX search response
```xml
<?xml version="1.0" encoding="utf-8" ?>
<Service_SearchHotel>
    <SearchHotel_Response>
        <Hotel HotelId="WSMA0511000113" HotelName="Amari Boulevard" Rating="4" Currency="USD" MarketName="All Market" dtCheckIn="2016-05-01" dtCheckOut="2016-05-05" CancelPolicyId="" InternalCode="CL004" avail="True">
            <RoomCateg Code="WSMA09040029" Name="SUPREME" NetPrice="192.00" GrossPrice="192.00" CommPrice="0.00" Price="192.00" BFType="RO">
                <RoomType TypeName="Twin" NumRooms="2" TotalPrice="192.00" avrNightPrice="48.00" RTGrossPrice="192.00" RTCommPrice="0.00" RTNetPrice="192.00">
                    <Rate offSet="0" NightPrice="64.00">
                        <RoomRate>
                            <RoomSeq No="1" AdultNum="2" ChildNum="0" ChildAge1="" ChildAge2="" RoomPrice="32.00" MinstayPrice="0.00" CompulsoryPrice="0.00" SupplementPrice="0.00" PromotionBFPrice="0" EarlyBirdDiscount="0.00" CommissionPrice="0.00" sRoomType="N"/>
                            <RoomSeq No="2" AdultNum="2" ChildNum="0" ChildAge1="" ChildAge2="" RoomPrice="32.00" MinstayPrice="0.00" CompulsoryPrice="0.00" SupplementPrice="0.00" PromotionBFPrice="0" EarlyBirdDiscount="0.00" CommissionPrice="0.00" sRoomType="N"/>
                        </RoomRate>
                        <RoomRateInfo>
                            <Child MinAge="0" MaxAge="0" Desc=""/>
                            <Minstay MSDay="0.00" MSType="NONE" MSRate="0.00"/>
                            <Compulsory Name="NONE"/>
                            <Supplement Name="NONE"/>
                            <Promotion Name="NONE" Value="False" PromoCode=""/>
                            <EarlyBird EBType="NONE" EBRate="0.00"/>
                            <Commission CommType="NONE" CommRate="0.00"/>
                        </RoomRateInfo>
                    </Rate>
                    ...
                ...
            ...
    </SearchHotel_Response>
</Service_SearchHotel>     
```
 
### API search response

```json
{
  "packages": {
    "WSMA0511000113": [
      {
        "partner": "travflex",
        "supplier": "travflex",
        "supplier_sell_rate": {
          "currency": "USD",
          "value": "64.00"
        },
        "supplier_hotel_id": "WSMA0511000113",
        "supplier_room_details": {
          "food": "RO",
          "room_type": "Twin",
          "room_view": "",
          "description": "Twin",
          "category_id": "WSMA09040029",
          "category_name": "SUPREME"       
        },
        "supplier_details": {
          "rooms": "[{\"Seq\":\"1\",\"AdultNum\":\"2\",\"ChildNum\":\"0\",\"ChildAge1\":\"\",\"ChildAge2\":\"\",\"RoomPrice\":\"32.00\",\"MinstayPrice\":\"0.00\",\"CompulsoryPrice\":\"0.00\",\"SupplementPrice\":\"0.00\",\"CommissionPrice\":\"0.00\",\"EarlyBirdDiscount\":\"0.00\",\"PromotionBFPrice\":\"0\",\"SRoomType\":\"N\"},{\"Seq\":\"2\",\"AdultNum\":\"2\",\"ChildNum\":\"0\",\"ChildAge1\":\"\",\"ChildAge2\":\"\",\"RoomPrice\":\"32.00\",\"MinstayPrice\":\"0.00\",\"CompulsoryPrice\":\"0.00\",\"SupplementPrice\":\"0.00\",\"CommissionPrice\":\"0.00\",\"EarlyBirdDiscount\":\"0.00\",\"PromotionBFPrice\":\"0\",\"SRoomType\":\"N\"}]",
          "flag_avail": "True",
          "internal_code": "CL004"
        },
        "is_bundled_rate": false
      }
      ...      
    ],
    "WSMA0511000127": [
      {
        "partner": "travflex",
        "supplier": "travflex",
        "supplier_sell_rate": {
          "currency": "USD",
          "value": 161.30
        },
        "zumata_commission": 10.00,
        "supplier_hotel_id": "WSMA0511000127",
        "supplier_room_details": {
          "food": "RO",
          "room_type": "Twin",
          "room_view": "",
          "category_id": "WSMA09040029",
          "category_name": "SUPREME"
        },
        "supplier_details": {
          "rooms": "[{\"Seq\":\"1\",\"AdultNum\":\"2\",\"ChildNum\":\"0\",\"ChildAge1\":\"\",\"ChildAge2\":\"\",\"RoomPrice\":\"80.65\",\"MinstayPrice\":\"0.00\",\"CompulsoryPrice\":\"0.00\",\"SupplementPrice\":\"0.00\",\"CommissionPrice\":\"0.00\",\"EarlyBirdDiscount\":\"0.00\",\"PromotionBFPrice\":\"0\",\"SRoomType\":\"N\"},{\"Seq\":\"2\",\"AdultNum\":\"2\",\"ChildNum\":\"0\",\"ChildAge1\":\"\",\"ChildAge2\":\"\",\"RoomPrice\":\"80.65\",\"MinstayPrice\":\"0.00\",\"CompulsoryPrice\":\"0.00\",\"SupplementPrice\":\"0.00\",\"CommissionPrice\":\"0.00\",\"EarlyBirdDiscount\":\"0.00\",\"PromotionBFPrice\":\"0\",\"SRoomType\":\"N\"}]",
          "flag_avail": "True",
          "internal_code": "CL004"
        },
        "is_bundled_rate": false
      }
      ...
```

* `hotel_id(pkg)`.zumata_commission is a comission, type `Float`, omit if empty
* `hotel_id(pkg)`.supplier_sell_rate.value `Float` with format `YYYY-MM-DD`
* `hotel_id(pkg)`.supplier_room_details object used in View Cancellation Policy request
* `hotel_id(pkg)`.supplier_room_details.food is a food type
* `hotel_id(pkg)`.supplier_room_details.room_type/desription is a room type
* `hotel_id(pkg)`.supplier_room_details.category_id used in booking request
* `hotel_id(pkg)`.supplier_room_details.category_name is a room category name
* `hotel_id(pkg)`.supplier_details.flag_avail is an availability flag
* `hotel_id(pkg)`.supplier_details.internal_code used in booking request

Each package element corresponds to a RoomType element supplier's response.
In our case only first element of Rate used to produce answer. The rest of elements are omitted.

## Cancellation Policy
### Additional Info API Request
All charge types presented as Percentage in field `penalty_percentage`

```json
{
    "params": {
        "check_in_date": "2016-04-29",
        "check_out_date": "2016-05-01",
        "cancel_policy_id": ""
    },
    "package": {
        "supplier_hotel_id": "WSMA0511000113",
        "supplier_sell_rate": {
            "currency": "USD",
            "value": 95.00
        },
        "supplier_details": {
            "rooms": "[{\"Seq\":1,\"AdultNum\":1,\"ChildNum\":1,\"ChildAge1\":8,\"ChildAge2\":0,\"RoomPrice\":60,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"COA\"},{\"Seq\":2,\"AdultNum\":1,\"ChildNum\":0,\"ChildAge1\":0,\"ChildAge2\":0,\"RoomPrice\":35,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"N\"}]",
            "flag_avail": "True",
            "internal_code": "CL004"
        },
    },
    "credentials": {
        "partner_id": "<partner_id>",
        "username": "<username>",
        "password": "<password>", 
        "url": "http://xml.travflex.com/11WS_SP2_1/ServicePHP/ViewCancelPolicy.php",
        "pax_passport": "<pax_passport_code>"
    }        
}
```

* params.check_in_date* string with format `YYYY-MM-DD`
* params.check_out_date* string with format `YYYY-MM-DD`
* params.cancel_policy_id* recieved from Search request, type `String`
* params.partner_destination_ids* is an array of `String` where first and second elements correspondly 
    * Country code
    * City code
* package.supplier_details.internal_code* is Supplier internal code involved into View Cancellation Policy request, type `String`
* package.supplier_details.flag_avail* is availability room. This value will refer to search response. Also involved into View Cancellation Policy request, type `String`
* credentials.username* is a username provided by supplier, type `String`
* credentials.password* supplier url, type `String`
* credentials.url* supplier url for cancellation policy, type `String`
* credentials.pax_passport* guest passport code, type `String`

### Additional Info API Response

```json
{
  "cancellation_policy": {
    "cancellation_policies": [
      {
        "original_rate": 95,
        "description": "Room Category : DELUXE , Meal Type : ALL Meal",
        "penalty_percentage": 100,
        "date_from": "2016-04-01",
        "date_to": "2016-04-24"
      },
      {
        "original_rate": 95,
        "description": "Room Category : DELUXE , Meal Type : ALL Meal",
        "penalty_percentage": 100,
        "date_from": "2016-04-01",
        "date_to": "2016-04-28"
      },
      {
        "original_rate": 95,
        "description": "Room Category : SUPREME , Meal Type : ABBF",
        "penalty_percentage": 6.32,
        "date_from": "2016-04-01",
        "date_to": "2016-04-10"
      },
      {
        "original_rate": 95,
        "description": "Room Category : SUPREME , Meal Type : ABBF",
        "penalty_percentage": 10.53,
        "date_from": "2016-04-01",
        "date_to": "2016-04-16"
      },
      {
        "original_rate": 95,
        "description": "Room Category : SUPREME , Meal Type : ABBF",
        "penalty_percentage": 100,
        "date_from": "2016-04-01",
        "date_to": "2016-04-24"
      },
      {
        "original_rate": 95,
        "description": "Room Category : ALL Room , Meal Type : ALL Meal",
        "penalty_percentage": 50,
        "date_from": "2016-04-01",
        "date_to": "2016-04-21"
      },
      {
        "original_rate": 95,
        "description": "Room Category : ALL Room , Meal Type : ALL Meal",
        "penalty_percentage": 100,
        "date_from": "2016-04-01",
        "date_to": "2016-04-28"
      }
    ]
  }
}
```

## Booking
### Booking API Request

```json
{
    "params": {
        "check_in_date": "2016-05-12",
        "check_out_date": "2016-05-13",
        "room_count": 1,
        "adult_count": 2,
        "pax_passport": "<passport_code>"
        "partner_destination_ids": ["MA05110001","MA05110041"]
    },
    "guest": {
        "first_name": "Luke",
        "last_name": "Skywalker",
        "salutation": "Jedi."
    },
    "package":{
        "supplier_sell_rate": {
          "currency": "USD",
          "value": 64.00
        },
        "supplier_hotel_id": "WSMA0511000113",
        "supplier_room_details": {
          "category_id": "WSMA09040029",
          "category_name": "SUPREME",
          "room_type": "Twin",
          "food": "RO",
          "room_view": ""
        },
        "supplier_details": {
            "rooms": "[{\"Seq\":1,\"AdultNum\":2,\"ChildNum\":0,\"ChildAge1\":0,\"ChildAge2\":0,\"RoomPrice\":32,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"N\"},{\"Seq\":2,\"AdultNum\":2,\"ChildNum\":0,\"ChildAge1\":0,\"ChildAge2\":0,\"RoomPrice\":32,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"N\"}]",
            "flag_avail": "True",
            "internal_code": "CL004"
        },
        "is_bundled_rate": false
    },
    "credentials": {
        "partner_id": "<partner_id>",
        "username": "<username>",
        "password": "<password>", 
        "url": "http://xml.travflex.com/11WS_SP2_1/ServicePHP/BookHotel.php"
    }
}
```

### TRAVFLEX Booking Request

```xml
<Service_BookHotel>
    <AgentLogin>
        <AgentId>partnerId</AgentId>
        <LoginName>username</LoginName>
        <Password>password</Password>
    </AgentLogin>
    <BookHotel>
        <OSRefNo>fZDprxyLfA</OSRefNo>
        <PaxPassport>MA05110059</PaxPassport>
        <HotelList InternalCode="CL004" Seq="1" flagAvail="True">
            <HotelId>WSMA0511000113</HotelId>
            <RoomCatg BFType="RO" CatgId="WSMA09040029" CatgName="SUPREME" checkIn="2016-05-12" checkOut="2016-05-13">
                <RoomType Price="32" RQBedChild="N" Seq="1" TypeName="Twin">
                    <AdultNum>2</AdultNum>
                    <ChildAges/>
                    <PaxInformation>
                        <PaxInfo Id="1">Skywalker Luke, Jedi.</PaxInfo>
                        <PaxInfo Id="2">Skywalker Luke, Jedi.</PaxInfo>
                    </PaxInformation>
                </RoomType>
                <RoomType Price="32" RQBedChild="N" Seq="2" TypeName="Twin">
                    <AdultNum>2</AdultNum>
                    <ChildAges/>
                    <PaxInformation>
                        <PaxInfo Id="1">Skywalker Luke, Jedi.</PaxInfo>
                        <PaxInfo Id="2">Skywalker Luke, Jedi.</PaxInfo>
                    </PaxInformation>
                </RoomType>
            </RoomCatg>
        </HotelList>
        <FinishBook>Y</FinishBook>
    </BookHotel>
</Service_BookHotel>
```

### TRAVFLEX Booking Response

```xml
<?xml version="1.0" encoding="utf-8" ?>
<BookingHotel>
    <BookHotel_Response>
        <ResNo Status="CONF" PaxPassport="WSMA05110059" OSRefNo="fZDprxyLfA" FinishBook="Y" RPCurrency="USD">ZMGHMA160411710</ResNo>
        <CompleteService>
            <HBookId CanAmend="Y" Id="HBMA1604000616" VoucherNo="VBMA1604001204" VoucherDt="2016-04-19" Status="CONF" InternalCode="CL004">
                <HotelId HotelName="Amari Boulevard">WSMA0511000113</HotelId>
                <Period FromDt="2016-05-12" ToDt="2016-05-13"/>
                <RoomCatgInfo>
                    <RoomCatg CatgId="WSMA09040029" CatgName="SUPREME" Market="All Market" Avail="Y" BFType="RO">
                        <Room>
                            <SeqRoom ServiceNo="MA1604000785" RoomType="Twin" SeqNo="1" AdultNum="2"  >
                                <PriceInfomation>
                                    <NightPrice Offset="0">
                                        <Accom Price="32.00" />
                                        <Child MinAge="" MaxAge="" Info=""/>
                                        <Minstay MSDay="0.00" MSType="NONE" MSRate="0.00" MSPrice="" />
                                        <Compulsory Name="NONE" Price="0.00"/>
                                        <Supplement Name="NONE" Price="0.00"/>
                                        <Promotion Name="NONE" Value="False" BFPrice="0" PromoCode=""/>
                                        <EarlyBird EBType="NONE" EBRate="0.00" EBPrice="0.00"/>
                                        <Commission CommType="NONE" CommRate="0.00" CommPrice="0.00"/>
                                    </NightPrice>
                                </PriceInfomation>
                                <TotalPrice>32.00</TotalPrice>
                                <CommissionPrice>0.00</CommissionPrice>
                                <NetPrice>32.00</NetPrice>
                                <PaxInformation>
                                    <GuestName>Skywalker Luke, NONE</GuestName>
                                    <GuestName>Skywalker Luke, NONE</GuestName>
                                </PaxInformation>
                            </SeqRoom>
                            <SeqRoom ServiceNo="MA1604000786" RoomType="Twin" SeqNo="2" AdultNum="2"  >
                                <PriceInfomation>
                                    <NightPrice Offset="0">
                                        <Accom Price="32.00" />
                                        <Child MinAge="" MaxAge="" Info=""/>
                                        <Minstay MSDay="0.00" MSType="NONE" MSRate="0.00" MSPrice="" />
                                        <Compulsory Name="NONE" Price="0.00"/>
                                        <Supplement Name="NONE" Price="0.00"/>
                                        <Promotion Name="NONE" Value="False" BFPrice="0" PromoCode=""/>
                                        <EarlyBird EBType="NONE" EBRate="0.00" EBPrice="0.00"/>
                                        <Commission CommType="NONE" CommRate="0.00" CommPrice="0.00"/>
                                    </NightPrice>                
                                </PriceInfomation>
                                <TotalPrice>32.00</TotalPrice>
                                <CommissionPrice>0.00</CommissionPrice>
                                <NetPrice>32.00</NetPrice>
                                <PaxInformation>
                                    <GuestName>Skywalker Luke, NONE</GuestName>
                                    <GuestName>Skywalker Luke, NONE</GuestName>
                                </PaxInformation>
                            </SeqRoom>
                        </Room>
                    </RoomCatg>
                </RoomCatgInfo>
                <RequestDes>
                    <![CDATA[]]>
                </RequestDes>
                <Message>
                    <![CDATA[xml.travflex.com]]>
                </Message>
            </HBookId>
        </CompleteService>
        <UnCompleteService></UnCompleteService>
    </BookHotel_Response>
</BookingHotel>
```

### Booking API Response

```json
{
  "supplier_details": {
    "booking_reference": "ZMGHMA160411712"
  },
  "confirmation_details": {
    "customer_reference": "ZMGHMA160411712"
  },
  "status": {
    "code": 0,
    "description": ""
  }
}
```
## Cancel Reservation
### Cancel Reservation API Request

```json
{
    "additional_references": {
        "result_no": "<res_no>",
        "BookingReference": "<booking_reference>"
    },
    "credentials": {
        "partner_id": "<partner_id>",
        "username": "<username>",
        "password": "<password>", 
        "url": "http://xml.travflex.com/11WS_SP2_1/ServicePHP/CancelRSVN.php"
    }
}
```

* additional_references requred
* additional_references.result_no optionally
* additional_references.BookingReference mandatory field
* credentials.partner_id mandatory fields
* credentials.username mandatory fields
* credentials.password mandatory fields
* credentials.url mandatory fields

### TRAVFLEX Cancel Reservation Request

```xml
<Service_CancelRSVN>
    <AgentLogin>
        <AgentId>partnerId</AgentId>
        <LoginName>username</LoginName>
        <Password>password</Password>
    </AgentLogin>
    <CancelReservation>
        <ResNo>XXXXMA130900008</ResNo>
        <OSRefNo/>
        <Hbooking>HBMA13090008</Hbooking>
    </CancelReservation>
</Service_CancelRSVN>
```

### TRAVFLEX Cancel Reservation Response

```xml
<?xml version="1.0" encoding="utf-8" ?>
<Service_CancelRSVN>
    <CancelReservation_Response>
        <ResNo>XXXXMA130900008</ResNo>
        <OSRefNo>P20141020001</OSRefNo>
        <Hbooking>HBMA1410000276</Hbooking>
        <CancelHbooking>HCMA1410000109</CancelHbooking> 
        <CancelResult>True</CancelResult>
        <Policies>
            <RoomCatgCode BFType="RO">WSMA05110034</RoomCatgCode>
            <ExCancelDays>21</ExCancelDays>
            <ChargeType>AMOUNT</ChargeType>
            <ChargeRate>2500.00</ChargeRate>
            <ChargePrice Price="2500.00 " Currency="THB" />
        </Policies>
    </CancelReservation_Response>
</Service_CancelRSVN>
```

### Cancel Reservation API Response

```json
{
    "penalty": {
        "Currency": "USD",
        "Value": 12.50
    },
    "supplier_details": {
        "sdBookingReference": "HBMA1410000276",
        "sdCancellationReference": "HCMA1410000109",
        "sdResNo": "XXXXMA130900008",
        "sdOSRefNo": "P20141020001"
    }
}
```

## Error handling
### Client error
If clients send an invalid json request, we return http status code 400

```json
{
    "message": "invalid character '}' looking for beginning of value"
}
```

If clients send a valid request but miss some fields, we return http status code 422

```json
{
    "message": "Validation failure",
    "errors": [
        {
            "field": "adult_count",
            "code": "missing"
        }
    ]
}
```
### Partner error
If partner is down or unreachable or incorrect url or connection timeout, we return http status code 500.

```json
{
    "message": "EOF"
}
```

If partner returns invalid response it's invalid endpoint, we return http status code 500.

```json
{
    "message": "expected element type <ExpectedXML_Element_Name> but have <RecievedXML_Element_Name>"
}
```