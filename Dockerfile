FROM golang:1.5

ADD . /go/src/bitbucket.org/papazz/travflex

WORKDIR /go/src/bitbucket.org/papazz/travflex

RUN go get github.com/tools/godep
RUN godep restore
RUN godep go install bitbucket.org/papazz/travflex

EXPOSE 3002
CMD /go/bin/travflex