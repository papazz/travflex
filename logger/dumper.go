package logger

import (
	"fmt"
	"log"
	"os"
	"path"
	"time"

	"github.com/pborman/uuid"
)

// RecordDumper is the dump interface
type RecordDumper interface {
	Dump([]byte) error
}

func createDirsIfRequired(filepath string) error {
	return os.MkdirAll(path.Dir(filepath), 0777)
}

// FileRecordDumper satisfies the RecordDumper interface
type FileRecordDumper struct {
	DumpDir      string
	DirContext   string
	FilePrefix   string
	FileUniqueID string
}

// Dump dumps the bytes to the file
func (fd *FileRecordDumper) Dump(recordBytes []byte) error {

	if fd.FileUniqueID == "" {
		fd.FileUniqueID = uuid.New()
	}

	filename := fd.FilePrefix + fd.FileUniqueID + ".json"
	fmt.Println(filename)
	toplevelDir := fmt.Sprintf("%s_%s", time.Now().Format("20060102_15H"), fd.DirContext)
	filepath := path.Join(
		path.Clean(fd.DumpDir),
		toplevelDir,
		filename,
	)

	err := createDirsIfRequired(filepath)
	if err != nil {
		log.Printf("ERROR: Unable to log request/response during creating directories: %v", filepath)
		return err
	}

	file, err := os.OpenFile(filepath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0660)
	if err != nil {
		log.Printf("ERROR: Unable to log request/response during open file: %v", filepath)
		return err
	}
	defer file.Close()

	_, err = file.Write(recordBytes)
	if err != nil {
		log.Printf("ERROR: Unable to log request/response during writing: %v", filepath)
		return err
	}

	return nil
}

// NewRecordDumper is the global instance
var NewRecordDumper func(*Options) RecordDumper

func init() {
	NewRecordDumper = func(opts *Options) RecordDumper {
		return &FileRecordDumper{
			DumpDir:      opts.DumpDir,
			DirContext:   opts.DirContext,
			FilePrefix:   opts.FilePrefix,
			FileUniqueID: opts.FileUniqueID,
		}
	}
}
