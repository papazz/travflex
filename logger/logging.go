package logger

import (
	"bitbucket.org/papazz/travflex/app"
	"github.com/pborman/uuid"
	"log"
	"net/http"
	"os"
	"strings"
)

// LoggingHandler sets up HTTP logger
func LoggingHandler(env *app.Env) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if os.Getenv("HTTP_LOGGING_ENABLED") == "1" {
				eventID := r.Header.Get("Event-ID")
				if eventID == "" {
					log.Println("event ID is missing")
					eventID = uuid.New()
				}

				dumpDir := os.Getenv("HTTP_LOGGING_DIR")
				if dumpDir == "" {
					dumpDir = "/tmp"
				}

				options := Options{
					DumpDir:      dumpDir,
					DirContext:   strings.Replace(r.URL.Path, "/", "", -1),
					FilePrefix:   "travflex_travflex_",
					FileUniqueID: eventID,
				}

				clientWrapper := Logger(options)

				env.HTTPLogger = clientWrapper(env.HTTPClient)
			} else {
				env.HTTPLogger = env.HTTPClient
			}

			next.ServeHTTP(w, r)
		})
	}
}
