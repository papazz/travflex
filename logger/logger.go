package logger

import (
	"bitbucket.org/papazz/travflex/app"
	"encoding/json"
	"log"
	"net/http"
	"time"
)

// Options is the logger options
type Options struct {
	DumpDir      string
	DirContext   string
	FilePrefix   string
	FileUniqueID string
}

// Logger is the entrance
func Logger(opts Options) app.ClientWrapper {

	clientWrapper := func(c app.HTTPClient) app.HTTPClient {
		// Define a clientFunc
		clientFunc := func(r *http.Request) (res *http.Response, err error) {
			reqData, err := captureRequest(r)
			if err != nil {
				return nil, err
			}

			startTime := time.Now()
			res, err = c.Do(r)
			if err != nil {
				return nil, err
			}
			endTime := time.Now()

			resData, err := captureResponse(res)
			if err != nil {
				return nil, err
			}

			go record(opts, reqData, resData, startTime, endTime.Sub(startTime))

			return res, err
		}
		// Convert clientFunc to app.ClientFunc
		// app.ClientFunc satisfy app.Client interface

		return app.ClientFunc(clientFunc)
	}

	return clientWrapper
}

type requestDump struct {
	Request     *requestSnapshot  `json:"request"`
	Response    *responseSnapshot `json:"response"`
	RequestedAt time.Time         `json:"requested_at"`
	LatencyMs   int64             `json:"latency_ms"`
}

func record(opts Options, req *requestSnapshot, res *responseSnapshot, requestedAt time.Time, duration time.Duration) {
	defer func() {
		if r := recover(); r != nil {
			log.Println("ERROR: Unable to log request/response as json:")
		}
	}()

	data, err := json.MarshalIndent(&requestDump{
		Request:     req,
		Response:    res,
		RequestedAt: requestedAt,
		LatencyMs:   duration.Nanoseconds() / 1000000,
	}, "", "\t")
	if err != nil {
		log.Printf("ERROR: Unable to log request/response as json during marshal: %v", err.Error())
		return
	}

	err = NewRecordDumper(&opts).Dump(data)
	if err != nil {
		log.Printf("ERROR: unable to dump HTTP record: %v", err.Error())
	}
}
