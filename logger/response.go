package logger

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

type responseSnapshot struct {
	Status   int                 `json:"status"`
	Headers  map[string][]string `json:"headers"`
	BodyData *string             `json:"body_data"`
	BodySize int64               `json:"body_size"`
}

func captureResponse(res *http.Response) (*responseSnapshot, error) {

	if res == nil || res.Body == nil {
		return nil, nil
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	res.Body = ioutil.NopCloser(bytes.NewBuffer(body))

	snapshot := &responseSnapshot{
		Status:  res.StatusCode,
		Headers: res.Header,
	}

	bodyStr := string(body)
	snapshot.BodyData = &bodyStr
	snapshot.BodySize = int64(len(body))

	return snapshot, nil
}
