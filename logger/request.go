package logger

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

type requestSnapshot struct {
	Method   string              `json:"method"`
	URL      string              `json:"url"`
	Headers  map[string][]string `json:"headers"`
	BodyData *string             `json:"body_data"`
	BodySize int64               `json:"body_size"`
}

func captureRequest(req *http.Request) (*requestSnapshot, error) {

	if req == nil {
		return nil, nil
	}

	snapshot := &requestSnapshot{
		Method:  req.Method,
		URL:     req.URL.String(),
		Headers: req.Header,
	}

	if req.Body != nil {
		body, err := ioutil.ReadAll(req.Body)
		if err != nil {
			return nil, err
		}

		req.Body = ioutil.NopCloser(bytes.NewBuffer(body))
		bodyStr := string(body)
		snapshot.BodyData = &bodyStr
		snapshot.BodySize = int64(len(body))

	}

	return snapshot, nil
}
