package app

import (
	"encoding/json"
	"encoding/xml"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"strings"
	"time"

	"bitbucket.org/papazz/travflex/data"
)

// CancelPolicy handler
func CancelPolicy(env *Env, w http.ResponseWriter, r *http.Request) error {
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		return NewHandlerError(http.StatusBadRequest, err.Error(), nil)
	}

	req := data.CpRequest{}
	if err := json.Unmarshal(body, &req); err != nil {
		return NewHandlerError(http.StatusBadRequest, InvalidRequest, nil)
	}

	if err := ValidateCp(req); err != nil {
		return err
	}

	res, err := GetAddInfo(env.HTTPLogger, &req)
	if err != nil {
		return NewHandlerError(http.StatusInternalServerError, err.Error(), nil)
	}

	env.Render.JSON(w, http.StatusOK, res)

	return nil
}

// GetAddInfo intermediate function
func GetAddInfo(httpClient HTTPClient, r *data.CpRequest) (*data.CpResponse, error) {
	req := buildCpRequest(r)

	cpRes, err := cpCall(httpClient, req, &r.Credentials)
	if err != nil {
		return nil, err
	}

	res, err := processCpResult(cpRes, r)
	if err != nil {
		return nil, err
	}

	return &data.CpResponse{CPolicy: res}, nil
}

func cpCall(httpClient HTTPClient, cpReq *data.CpRequestXML, c *data.Credentials) (*data.CpResponseXML, error) {
	body, err := httpCall(httpClient, cpReq, c)
	if err != nil {
		return nil, err
	}

	var cpRes data.CpResponseXML
	err = xml.Unmarshal(body, &cpRes)
	if err != nil {
		return nil, err
	}

	return &cpRes, nil
}

func processCpResult(res *data.CpResponseXML, req *data.CpRequest) (*data.CancellationPolicy, error) {
	// slice of policies converted to required format
	var cpBands []data.CPBand
	var remarks string

	// Policies sort
	// reverse sort needs to start policy chaining from high ExCancelDate to low
	sort.Sort(sort.Reverse(res.VCPR.Policies))
	pLength := len(res.VCPR.Policies.List)
	for i := 0; i < pLength; i++ {
		p := res.VCPR.Policies.List[i]
		nxp := data.Policy{}
		// supplier has width policy date range
		// chaining policy band process
		// check index out of bounds
		if pLength > i+1 {
			// next policy element
			nxp = res.VCPR.Policies.List[i+1]
		}

		// Filtering out unwanted policies by room category code
		// for our RoomCategCode or for regular policy
		if req.Package.Details[data.KCatID] != p.RoomCatgCode.Value {
			continue
		}

		var remarksSlice []string
		checkInDate, _ := DateToRFC3339(req.Params.CheckInDate)
		dateFrom := checkInDate.AddDate(0, 0, -(p.ExCancelDays))

		if isNonRef(&p) {
			dateFrom, _ = DateToRFC3339(time.Now().Format(TimeLayout))
		}

		fd, _ := time.Parse(TimeLayout, p.FromDate)
		startDate, err := ToWorstCaseTimezone(fd.Format(time.RFC3339))
		if err != nil {
			log.Println("unable to parse date to worst case timezone: ", fd.Format(time.RFC3339))
			continue
		}
		// DateTo time
		var td time.Time
		// when Policies List ended, data.Policy{} will be nil
		if nxp == (data.Policy{}) {
			td = checkInDate
		} else {
			// set DateTo equal to start next policy date
			td = checkInDate.AddDate(0, 0, -(nxp.ExCancelDays))
		}
		// cast to worst case
		endDate, err := ToWorstCaseTimezone(td.Add(-time.Hour * 10).Format(time.RFC3339))
		if err != nil {
			log.Println("unable to parse date to worst case timezone: ", td.Format(time.RFC3339))
			continue
		}
		// skip policy that not actually yet
		if startDate.After(checkInDate) {
			continue
		}
		// Close the gap by adding one day to the endDate.
		// Make 1s gap as well.
		endDate = endDate.AddDate(0, 0, 1).Add(-1 * time.Second)

		// when policy not started yet or policies item is last set DateTo = checkInDate
		if endDate.After(checkInDate) || nxp == (data.Policy{}) {
			endDate = checkInDate
		}

		var nRates []float64
		decNRates := json.NewDecoder(strings.NewReader(req.Package.Details[data.KRates]))
		decNRates.Decode(nRates)

		b := data.CPBand{
			PenaltyPercentage: CalcPenaltyPercentage(&p, req.Package.SellRate.Value, nRates),
			DateTo:            endDate,
			DateFrom:          dateFrom,
		}
		// in case of RoomCateg exist, use special policy only
		if req.Package.Details[data.KCatID] == p.RoomCatgCode.Value {
			// one special policy for response
			cpBands = []data.CPBand{}
			remarksSlice = []string{}

			b.DateTo = checkInDate
			cpBands = append(cpBands, b)
			remarksSlice = append(remarksSlice, p.Desc.Text)

			return &data.CancellationPolicy{CPBands: cpBands, Remarks: remarks}, nil
		}

		// build reqular policy
		cpBands = append(cpBands, b)
		remarksSlice = append(remarksSlice, p.Desc.Text)
		removeDuplicateString(&remarksSlice)
		remarks = strings.Join(remarksSlice, ". ")

	}

	return &data.CancellationPolicy{CPBands: cpBands, Remarks: remarks}, nil
}

func buildCpRequest(req *data.CpRequest) *data.CpRequestXML {
	return &data.CpRequestXML{
		Agent: data.Agent{
			AgentID:   req.Credentials.PartnerID,
			LoginName: req.Credentials.Login,
			Password:  req.Credentials.Pass,
		},
		VCPR: data.VCPRReq{
			PaxPassport:  req.Credentials.PaxPass,
			HotelID:      data.HID(req.Package.HotelID),
			InternalCode: req.Package.Details[data.KICode],
			CheckInDate:  req.Params.CheckInDate,
			CheckOutDate: req.Params.CheckOutDate,
			RoomsInfo:    buildCpRoomsInfo(req),
			FlagAvail:    req.Package.Details[data.KFAvail],
			CpID:         req.Package.Details[data.KCPolicyID],
		},
	}
}

func buildCpRoomsInfo(r *data.CpRequest) []data.CpRoomInfo {
	var s []data.CpRoomInfo

	for _, rs := range decRooms(r.Package.Details[data.KRooms]).RoomSeq {
		ri := data.CpRoomInfo{
			SeqNo:      rs.Seq,
			Adults:     rs.AdultNum,
			ChildAges:  buildChildAges(buildChildList(rs.ChildNum, rs.ChildAge1, rs.ChildAge2)),
			RQBedChild: data.RQBedChildDefault,
		}
		s = append(s, ri)
	}

	return s
}

// CalcPenaltyPercentage calculates charge percentage
func CalcPenaltyPercentage(p *data.Policy, sellRate float64, nRates []float64) float64 {
	if isNonRef(p) {
		return FullCharge
	}
	switch p.ChargeType {
	case "Percent":
		return roundFloat(p.ChargeRate)
	case "Amount":
		return roundFloat(p.ChargeRate * FullCharge / sellRate)
	case "Full Charge":
		return FullCharge
	case "Night/Room":
		return nightRoomPercentage(p.ChargeRate, sellRate, nRates)
	default:
		return FullCharge
	}
}

func isNonRef(p *data.Policy) bool {
	if p.ExCancelDays >= NonRefExCancelDays {
		return true
	}
	return false
}

func nightRoomPercentage(nights, sellRate float64, nRates []float64) float64 {
	if nights > 1.0 {
		var price float64
		if int(nights) > len(nRates) {
			return roundFloat(nRates[0] * nights * FullCharge / sellRate)
		}

		for i := 0; i < int(nights); i++ {
			price += nRates[i]
		}

		return roundFloat(price * FullCharge / sellRate)
	} else {
		return roundFloat(nRates[0] * FullCharge / sellRate)
	}
}
