package app_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"time"

	"bitbucket.org/papazz/travflex/app"
	"bitbucket.org/papazz/travflex/data"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Search", func() {
	Describe("API Search test", func() {
		Context("Case I: request is an invalid json", func() {
			It("should fail validation", func() {
				req, _ := http.NewRequest("POST", "/search", bytes.NewBufferString(`{"a":}`))
				w := httptest.NewRecorder()

				err := app.SearchHotel(env, w, req)

				Expect(err.(app.HandlerError).Message).To(Equal(`Invalid JSON was received by the server. An error occurred on the server while parsing the JSON text.`))
				Expect(err.(app.HandlerError).StatusCode).To(Equal(http.StatusBadRequest))
			})
		})

		Context("Case II: `room_count` missed in request", func() {
			It("should not pass validation", func() {
				req, _ := http.NewRequest("POST", "/search", bytes.NewBufferString(`{
						    "params": {
						        "check_in_date": "`+today.AddDate(0, 2, 0).Format(app.TimeLayout)+`",
						        "check_out_date": "`+today.AddDate(0, 2, 5).Format(app.TimeLayout)+`",
						        "adult_count": 4,
						        "partner_destination_ids": ["`+destCountry+`", "`+destCity+`"]
						    },
						    "credentials": {
						    	"partner_id": "`+partnerId+`",
						        "username": "`+login+`",
						        "password": "`+password+`", 
						        "url": "`+url+`",
						        "pax_passport": "`+paxPassport+`"
						    }
						}`))

				w := httptest.NewRecorder()

				err := app.SearchHotel(env, w, req)
				Expect(err.(app.HandlerError).Message).To(Equal(`Validation failure`))
				Expect(err.(app.HandlerError).Details).To(ConsistOf(
					app.ErrorDetail{Field: "room_count", Code: app.Missing},
					app.ErrorDetail{Field: "room_count", Code: app.OutOfRange},
				))
				Expect(err.(app.HandlerError).StatusCode).To(Equal(422))
			})

			It("should not pass validation", func() {
				req, _ := http.NewRequest("POST", "/search", bytes.NewBufferString(`{
				    "params": {
				        "check_in_date": "`+today.AddDate(0, 2, 0).Format(app.TimeLayout)+`",
				        "check_out_date": "`+today.AddDate(0, 2, 5).Format(app.TimeLayout)+`",
				        "adult_count": 4,
				        "partner_destination_ids": ["`+destCountry+`", "`+destCity+`"]
				    },
				    "credentials": {
				    	"partner_id": "`+partnerId+`",
				        "username": "`+login+`",
				        "password": "`+password+`", 
				        "url": "`+url+`",
				        "pax_passport": "`+paxPassport+`"
				    }
				}`))

				w := httptest.NewRecorder()

				err := app.SearchHotel(env, w, req)
				Expect(err.(app.HandlerError).Message).To(Equal(`Validation failure`))
				Expect(err.(app.HandlerError).Details).To(ConsistOf(
					app.ErrorDetail{Field: "room_count", Code: app.Missing},
					app.ErrorDetail{Field: "room_count", Code: app.OutOfRange},
				))
				Expect(err.(app.HandlerError).StatusCode).To(Equal(422))
			})
		})

		Context("Case III: `room_count` out_of_range in request", func() {
			It("should not pass validation", func() {
				cases := []struct {
					roomCount int
				}{
					{-1}, {0}, {11},
				}
				for _, c := range cases {
					req, _ := http.NewRequest("POST", "/search", bytes.NewBufferString(fmt.Sprintf(`{
						    "params": {
						        "check_in_date": "`+today.AddDate(0, 2, 0).Format(app.TimeLayout)+`",
						        "check_out_date": "`+today.AddDate(0, 2, 5).Format(app.TimeLayout)+`",
						        "adult_count": 4,
						        "room_count": %d,
						        "partner_destination_ids": ["`+destCountry+`", "`+destCity+`"]
						    },
						    "credentials": {
						    	"partner_id": "`+partnerId+`",
						        "username": "`+login+`",
						        "password": "`+password+`", 
						        "url": "`+url+`",
						        "pax_passport": "`+paxPassport+`"
						    }
						}`, c.roomCount)))

					w := httptest.NewRecorder()

					err := app.SearchHotel(env, w, req)
					Expect(err.(app.HandlerError).Message).To(Equal(`Validation failure`))
					Expect(err.(app.HandlerError).Details).Should(Or(
						ConsistOf(
							app.ErrorDetail{Field: "room_count", Code: app.Missing},
							app.ErrorDetail{Field: "room_count", Code: app.OutOfRange}),
						ConsistOf(
							app.ErrorDetail{Field: "room_count", Code: app.OutOfRange}),
					))
					Expect(err.(app.HandlerError).StatusCode).To(Equal(422))
				}
			})
		})

		Context("Case IV: `credentials` missed in request", func() {
			It("should not pass validation", func() {
				req, _ := http.NewRequest("POST", "/search", bytes.NewBufferString(`{
						    "params": {
						        "check_in_date": "`+today.AddDate(0, 2, 0).Format(app.TimeLayout)+`",
						        "check_out_date": "`+today.AddDate(0, 2, 5).Format(app.TimeLayout)+`",
						        "room_count": 3,
						        "adult_count": 4,
						        "partner_destination_ids": ["`+destCountry+`", "`+destCity+`"]
						    }
						}`))
				w := httptest.NewRecorder()

				err := app.SearchHotel(env, w, req)

				Expect(err.(app.HandlerError).Message).To(Equal("Validation failure"))
				Expect(err.(app.HandlerError).StatusCode).To(Equal(422))
				Expect(err.(app.HandlerError).Details).To(ConsistOf(
					app.ErrorDetail{Field: "pax_passport", Code: app.Missing},
					app.ErrorDetail{Field: "partner_id", Code: app.Missing},
					app.ErrorDetail{Field: "username", Code: app.Missing},
					app.ErrorDetail{Field: "password", Code: app.Missing},
					app.ErrorDetail{Field: "url", Code: app.Missing},
				))
			})
		})

		Context("Case V: `check_in_date` and `check_out_date` are out of range", func() {
			It("should not pass validation", func() {
				cases := []struct {
					checkInDate, checkOutDate string
					code                      app.ErrorCode
				}{
					// Must be in the format of YYYY-MM-DD
					{today.AddDate(0, 2, 0).Format(time.RFC3339), today.AddDate(0, 2, 5).Format("2006-1-02"), app.Invalid},
					// check_in_date is equal check_out_date
					{today.AddDate(0, 2, 0).Format(app.TimeLayout), today.AddDate(0, 2, 0).Format(app.TimeLayout), app.OutOfRange},
					// check_in_date is after check_out_date
					{today.AddDate(0, 2, 0).Format(app.TimeLayout), today.AddDate(0, 2, -1).Format(app.TimeLayout), app.OutOfRange},
					// check_in_date and check_out_date are passed
					{today.AddDate(-2, 0, 0).Format(app.TimeLayout), today.AddDate(-2, 0, 5).Format(app.TimeLayout), app.OutOfRange},
				}

				for _, c := range cases {
					str := fmt.Sprintf(`{
						    "params": {
						        "check_in_date": "%v",
						        "check_out_date": "%v",
						        "adult_count": 4,
						        "room_count": 3,
						        "partner_destination_ids": ["`+destCountry+`", "`+destCity+`"]
						    },
						    "credentials": {
						    	"partner_id": "`+partnerId+`",
						        "username": "`+login+`",
						        "password": "`+password+`", 
						        "url": "`+url+`",
						        "pax_passport": "`+paxPassport+`"
						    }
						}`, c.checkInDate, c.checkOutDate)
					req, _ := http.NewRequest("POST", "/search", bytes.NewBufferString(str))
					w := httptest.NewRecorder()

					err := app.SearchHotel(env, w, req)

					Expect(err.(app.HandlerError).Message).To(Equal("Validation failure"))
					Expect(err.(app.HandlerError).StatusCode).To(Equal(422))
					Expect(err.(app.HandlerError).Details).To(ConsistOf(
						app.ErrorDetail{Field: "check_in_date", Code: c.code},
						app.ErrorDetail{Field: "check_out_date", Code: c.code},
					))
				}
			})
		})

		Context("Case VI: checkIn <-> checkOut duration more then 30 days", func() {
			It("should not pass validation", func() {
				cases := []struct {
					checkInDate, checkOutDate string
				}{
					{today.AddDate(0, 2, 0).Format(app.TimeLayout), today.AddDate(0, 2, 31).Format(app.TimeLayout)},
				}
				for _, c := range cases {
					str := fmt.Sprintf(`{
						    "params": {
						        "check_in_date": "%v",
						        "check_out_date": "%v",
						        "room_count": 3,
						        "adult_count": 4,
						        "partner_destination_ids": ["`+destCountry+`", "`+destCity+`"]
						    },
						    "credentials": {
						    	"partner_id": "`+partnerId+`",
						        "username": "`+login+`",
						        "password": "`+password+`", 
						        "url": "`+url+`",
						        "pax_passport": "`+paxPassport+`"
						    }
						}`, c.checkInDate, c.checkOutDate)
					req, _ := http.NewRequest("POST", "/search", bytes.NewBufferString(str))
					w := httptest.NewRecorder()

					err := app.SearchHotel(env, w, req)

					Expect(err.(app.HandlerError).Message).To(Equal("Validation failure"))
					Expect(err.(app.HandlerError).StatusCode).To(Equal(422))
					Expect(err.(app.HandlerError).Details).To(ConsistOf(
						app.ErrorDetail{Field: "check_in_date", Code: app.OutOfRange},
						app.ErrorDetail{Field: "check_out_date", Code: app.OutOfRange},
					))
				}
			})
		})

		Context("Case VII: children range test", func() {
			It("should not pass validation", func() {
				cases := []struct {
					adultCount int
					roomCount  int
					childCount []int
				}{ // max child = 2*room_count
					{1, 1, []int{-1, 0}}, {1, 1, []int{0, 19}}, {40, 10, []int{1, 90}},
				}
				for _, c := range cases {
					str := fmt.Sprintf(`{
						    "params": {
						        "check_in_date": "`+today.AddDate(0, 2, 0).Format(app.TimeLayout)+`",
						        "check_out_date": "`+today.AddDate(0, 2, 5).Format(app.TimeLayout)+`",
						        "room_count": %d,
						        "adult_count": %d,
						        "children": [%d, %d],
						        "partner_destination_ids": ["`+destCountry+`", "`+destCity+`"]
						    },
						    "credentials": {
						        "partner_id": "`+partnerId+`",
						        "username": "`+login+`",
						        "password": "`+password+`", 
						        "url": "`+url+`",
						        "pax_passport": "`+paxPassport+`"
						    }
						}`, c.roomCount, c.adultCount, c.childCount[0], c.childCount[1])
					req, _ := http.NewRequest("POST", "/search", bytes.NewBufferString(str))
					w := httptest.NewRecorder()

					err := app.SearchHotel(env, w, req)

					Expect(err.(app.HandlerError).Message).To(Equal("Validation failure"))
					Expect(err.(app.HandlerError).StatusCode).To(Equal(422))
					Expect(err.(app.HandlerError).Details).Should(Or(
						ConsistOf(
							app.ErrorDetail{Field: "children", Code: app.Missing},
							app.ErrorDetail{Field: "children", Code: app.OutOfRange}),
						ConsistOf(
							app.ErrorDetail{Field: "children", Code: app.OutOfRange},
						),
						ConsistOf(
							app.ErrorDetail{Field: "children", Code: app.Invalid},
						)))
				}
			})
		})
	})

	Describe("API Search cassette. Check for valid search request", func() {
		var vcr *app.Client

		BeforeEach(func() {
			vcr = app.NewClient("cassettes")
		})

		Context("General", func() {

			It("should return success", func() {
				vcr.UseCassette("search-success", app.Once, func() {
					req := MockSearchParams()
					res, err := app.Search(vcr, req, app.EmptyString, app.EmptyString)
					Expect(err).To(BeNil())
					log.Printf("Hotel %v has %v packages\n", data.HID(hotel[0]), len(res.Packages[data.HID(hotel[0])]))

					for supplierHotelID, packages := range res.Packages {
						for _, pkg := range packages {
							Expect(pkg.Partner).To(Equal("travflex"))
							Expect(pkg.Supplier).To(Equal("travflex"))
							Expect(pkg.SellRate).NotTo(BeNil())
							Expect(pkg.HotelID).To(Equal(supplierHotelID))
							Expect(pkg.Details[data.KFood]).NotTo(Equal(0))
							Expect(pkg.Details).NotTo(BeNil())
						}
					}
				})
			})

			It("should return error when check-in-date is already passed", func() {
				vcr.UseCassette("search-invalid-date", app.Once, func() {
					req := MockSearchParams()
					req.Params.CheckInDate = today.AddDate(-1, 2, 0).Format(app.TimeLayout)
					req.Params.CheckOutDate = today.AddDate(-1, 2, 3).Format(app.TimeLayout)
					_, err := app.Search(vcr, req, app.EmptyString, app.EmptyString)
					Ω(err).To(HaveOccurred())
				})
			})

			It("should not return error when there is no result", func() {
				vcr.UseCassette("search-no-result", app.Once, func() {
					req := MockSearchParams()
					req.Params.CheckInDate = today.AddDate(50, 0, 0).Format(app.TimeLayout)
					req.Params.CheckOutDate = today.AddDate(50, 0, 5).Format(app.TimeLayout)
					res, err := app.Search(vcr, req, app.EmptyString, app.EmptyString)
					Expect(err).To(BeNil())
					Expect(len(res.Packages)).To(Equal(0))
				})
			})
		})

		Context("Multiple rooms package generation", func() {

			It("should generate packages from rooms with same room type", func() {
				vcr.UseCassette("search-multiple-rooms-1", app.Once, func() {
					req := MockSearchParams()
					ci := today.AddDate(0, 2, 0)
					co := today.AddDate(0, 2, 2)
					req.Params.CheckInDate = ci.Format(app.TimeLayout)
					req.Params.CheckOutDate = co.Format(app.TimeLayout)
					req.Params.AdultCount = 5
					req.Params.RoomCount = 5
					res, err := app.Search(vcr, req, app.EmptyString, app.EmptyString)
					Ω(err).ToNot(HaveOccurred())

					for _, pkgs := range res.Packages {
						for _, pkg := range pkgs {
							var rRate data.RoomRate
							d := json.NewDecoder(strings.NewReader(pkg.Details[data.KRooms]))
							err := d.Decode(&rRate.RoomSeq)
							Ω(err).ToNot(HaveOccurred())
							Ω(pkg.Supplier).Should(Equal("travflex"))
						}
					}
				})
			})

			It("should generate packages from rooms with same room type", func() {
				vcr.UseCassette("search-multiple-rooms-2", app.Once, func() {
					req := MockSearchParams()
					ci := today.AddDate(0, 1, 0)
					co := today.AddDate(0, 1, 10)
					req.Params.CheckInDate = ci.Format(app.TimeLayout)
					req.Params.CheckOutDate = co.Format(app.TimeLayout)
					req.Params.AdultCount = 18
					req.Params.Children = []int{7, 6, 5, 5, 3, 2, 5, 7}
					req.Params.RoomCount = 7
					res, err := app.Search(vcr, req, app.EmptyString, app.EmptyString)
					Ω(err).ToNot(HaveOccurred())
					Ω(len(res.Packages)).Should(BeNumerically(">", 0))
				})
			})
		})
	})
})
