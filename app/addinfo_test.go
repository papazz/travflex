package app_test

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"time"

	"bitbucket.org/papazz/travflex/app"
	"bitbucket.org/papazz/travflex/data"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Addinfo", func() {
	Describe("Functional tests", func() {
		Context("Case I: CalcPenaltyPercentage", func() {
			It("should success", func() {
				rooms := 3.0
				days := 5.0
				roomPrice := 13.80
				nightPrice := []float64{rooms * roomPrice} //, rooms * roomPrice / 2.0} // 41.40, 20.70. sum = 62.10
				sellRate := nightPrice[0] * days           // 3 rooms * 3 days
				p := &data.Policy{
					ChargeRate: 124.00,
					ChargeType: "Amount",
				}
				Expect(app.CalcPenaltyPercentage(p, sellRate, nightPrice)).To(Equal(59.90))

				p = &data.Policy{
					ChargeRate: 28,
					ChargeType: "Percent",
				}
				Expect(app.CalcPenaltyPercentage(p, sellRate, nightPrice)).To(Equal(28.00))

				// nights == len(nightRates) 1 rate
				p = &data.Policy{
					ChargeRate: 1,
					ChargeType: "Night/Room",
				}
				Expect(app.CalcPenaltyPercentage(p, sellRate, nightPrice)).To(Equal(20.00))

				// nights > len(nightRates) 1 rate
				p = &data.Policy{
					ChargeRate: 2,
					ChargeType: "Night/Room",
				}
				Expect(app.CalcPenaltyPercentage(p, sellRate, nightPrice)).To(Equal(40.00))

				// nights == len(nightRates) 2 rates
				nightPrice = []float64{rooms * roomPrice, rooms * roomPrice / 2.0}
				sellRate = nightPrice[0] * days
				p = &data.Policy{
					ChargeRate: 2,
					ChargeType: "Night/Room",
				}
				Expect(app.CalcPenaltyPercentage(p, sellRate, nightPrice)).To(Equal(30.00))
			})
		})
	})

	Describe("API Cancellation Policy test", func() {
		Context("Case I: request is an invalid json", func() {
			It("should fail validation", func() {
				req, _ := http.NewRequest("POST", "/additional_info", bytes.NewBufferString(`{"a":}`))
				w := httptest.NewRecorder()

				err := app.CancelPolicy(env, w, req)

				Expect(err.(app.HandlerError).Message).To(Equal(`Invalid JSON was received by the server. An error occurred on the server while parsing the JSON text.`))
				Expect(err.(app.HandlerError).StatusCode).To(Equal(http.StatusBadRequest))
			})
		})

		Context("Case II: `supplier_hotel_id` missed in request", func() {
			It("should not pass validation", func() {
				req, _ := http.NewRequest("POST", "/additional_info", bytes.NewBufferString(`{
					"params": {
				        "check_in_date": "`+today.AddDate(0, 2, 0).Format(app.TimeLayout)+`",
				        "check_out_date": "`+today.AddDate(0, 2, 5).Format(app.TimeLayout)+`",
				        "adult_count": 1,
				        "room_count": 1,
				        "partner_destination_ids": ["`+destCountry+`", "`+destCity+`"]
				    },
				    "package": {
				        "supplier_sell_rate": {
				            "currency": "USD",
				            "value": 95.00
				        },
				        "supplier_details": {
				          "rooms": "[{\"Seq\":1,\"AdultNum\":1,\"ChildNum\":1,\"ChildAge1\":8,\"ChildAge2\":0,\"RoomPrice\":60,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"COA\"},{\"Seq\":2,\"AdultNum\":1,\"ChildNum\":0,\"ChildAge1\":0,\"ChildAge2\":0,\"RoomPrice\":35,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"N\"}]",
				          "flag_avail": "True",
				          "internal_code": "`+icode+`",
				    	  "cancel_policy_id": "`+app.EmptyString+`",
				    	  "room_category_code": "WSMA05110034"
				        }
				    },
				    "credentials": {
				    	"partner_id": "`+partnerId+`",
				        "username": "`+login+`",
				        "password": "`+password+`",
				        "url": "`+urlCP+`",
						"pax_passport": "`+paxPassport+`"
				    }}`))
				w := httptest.NewRecorder()
				err := app.CancelPolicy(env, w, req)
				Expect(err.(app.HandlerError).Message).To(Equal(`Validation failure`))
				Expect(err.(app.HandlerError).Details).To(ConsistOf(
					app.ErrorDetail{Field: "supplier_hotel_id", Code: app.Missing},
				))
				Expect(err.(app.HandlerError).StatusCode).To(Equal(422))
			})
		})

		Context("Case III: `flag_avail internal_code` missed in request", func() {
			It("should not pass validation", func() {
				req, _ := http.NewRequest("POST", "/additional_info", bytes.NewBufferString(`{
					"params": {
				        "check_in_date": "`+today.AddDate(0, 2, 0).Format(app.TimeLayout)+`",
				        "check_out_date": "`+today.AddDate(0, 2, 5).Format(app.TimeLayout)+`",
				        "adult_count": 1,
				        "room_count": 1,
				        "partner_destination_ids": ["`+destCountry+`", "`+destCity+`"]
				    },
				    "package": {
				        "supplier_hotel_id": "WSMA0511000113",
				        "supplier_sell_rate": {
				            "currency": "USD",
				            "value": 95.00
				        },
				        "supplier_details": {
				          "rooms": "[{\"Seq\":1,\"AdultNum\":1,\"ChildNum\":1,\"ChildAge1\":8,\"ChildAge2\":0,\"RoomPrice\":60,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"COA\"},{\"Seq\":2,\"AdultNum\":1,\"ChildNum\":0,\"ChildAge1\":0,\"ChildAge2\":0,\"RoomPrice\":35,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"N\"}]"
				        }
				    },
				    "credentials": {
				    	"partner_id": "`+partnerId+`",
				        "username": "`+login+`",
				        "password": "`+password+`",
				        "url": "`+urlCP+`",
						"pax_passport": "`+paxPassport+`"
				    }}`))
				w := httptest.NewRecorder()
				err := app.CancelPolicy(env, w, req)
				Expect(err.(app.HandlerError).Message).To(Equal(`Validation failure`))
				Expect(err.(app.HandlerError).Details).To(ConsistOf(
					app.ErrorDetail{Field: "flag_avail", Code: app.Missing},
					app.ErrorDetail{Field: "internal_code", Code: app.Missing},
					app.ErrorDetail{Field: "night_rates", Code: app.Missing},
				))
				Expect(err.(app.HandlerError).StatusCode).To(Equal(422))
			})
		})

		Context("Case IV: `credentials` missed in request", func() {
			It("should not pass validation", func() {
				req, _ := http.NewRequest("POST", "/additional_info", bytes.NewBufferString(`{
					"params": {
				        "check_in_date": "`+today.AddDate(0, 2, 0).Format(app.TimeLayout)+`",
				        "check_out_date": "`+today.AddDate(0, 2, 5).Format(app.TimeLayout)+`",
				        "adult_count": 1,
				        "room_count": 1,
				        "partner_destination_ids": ["`+destCountry+`", "`+destCity+`"]
				    },
				    "package": {
				        "supplier_hotel_id": "WSMA0511000113",
				        "supplier_sell_rate": {
				            "currency": "USD",
				            "value": 95.00
				        },
				        "supplier_details": {
				          "rooms": "[{\"Seq\":1,\"AdultNum\":1,\"ChildNum\":1,\"ChildAge1\":8,\"ChildAge2\":0,\"RoomPrice\":60,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"COA\"},{\"Seq\":2,\"AdultNum\":1,\"ChildNum\":0,\"ChildAge1\":0,\"ChildAge2\":0,\"RoomPrice\":35,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"N\"}]",
				          "flag_avail": "True",
				          "internal_code": "`+icode+`",
				          "cancel_policy_id": "`+app.EmptyString+`",
				          "room_category_code": "WSMA05110034"
				        }
				    }}`))
				w := httptest.NewRecorder()
				err := app.CancelPolicy(env, w, req)
				Expect(err.(app.HandlerError).Message).To(Equal("Validation failure"))
				Expect(err.(app.HandlerError).StatusCode).To(Equal(422))
				Expect(err.(app.HandlerError).Details).To(ConsistOf(
					app.ErrorDetail{Field: "partner_id", Code: app.Missing},
					app.ErrorDetail{Field: "pax_passport", Code: app.Missing},
					app.ErrorDetail{Field: "username", Code: app.Missing},
					app.ErrorDetail{Field: "password", Code: app.Missing},
					app.ErrorDetail{Field: "url", Code: app.Missing},
				))
			})
		})

		Context("Case V: `supplier_sell_rate` missed in request", func() {
			It("should not pass validation", func() {
				req, _ := http.NewRequest("POST", "/additional_info", bytes.NewBufferString(`{
				    "params": {
				        "check_in_date": "`+today.AddDate(0, 2, 0).Format(app.TimeLayout)+`",
				        "check_out_date": "`+today.AddDate(0, 2, 5).Format(app.TimeLayout)+`",
				        "adult_count": 1,
				        "room_count": 1,
				        "partner_destination_ids": ["`+destCountry+`", "`+destCity+`"]
				    },
				    "package": {
				        "supplier_hotel_id": "WSMA0511000113",
				        "supplier_details": {
				          "rooms": "[{\"Seq\":1,\"AdultNum\":1,\"ChildNum\":1,\"ChildAge1\":8,\"ChildAge2\":0,\"RoomPrice\":60,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"COA\"},{\"Seq\":2,\"AdultNum\":1,\"ChildNum\":0,\"ChildAge1\":0,\"ChildAge2\":0,\"RoomPrice\":35,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"N\"}]",
				          "flag_avail": "True",
				          "internal_code": "`+icode+`",
				          "cancel_policy_id": "`+app.EmptyString+`",
				          "room_category_code": "WSMA05110034"
				        }
				    },
				    "credentials": {
				    	"partner_id": "`+partnerId+`",
				        "username": "`+login+`",
				        "password": "`+password+`", 
				        "url": "`+urlCP+`",
						"pax_passport": "`+paxPassport+`"
				    }        
				}`))

				w := httptest.NewRecorder()

				err := app.CancelPolicy(env, w, req)
				Expect(err.(app.HandlerError).Message).To(Equal(`Validation failure`))
				Expect(err.(app.HandlerError).Details).To(ConsistOf(
					app.ErrorDetail{Field: "currency", Code: app.Missing},
				))
				Expect(err.(app.HandlerError).StatusCode).To(Equal(422))
			})
		})

		Context("Case VI: `check_in_date` and `check_out_date` are out of range", func() {
			It("should not pass validation", func() {
				cases := []struct {
					checkInDate, checkOutDate string
					code                      app.ErrorCode
				}{
					// Must be in the format of YYYY-MM-DD
					{today.AddDate(0, 2, 0).Format(time.RFC3339), today.AddDate(0, 2, 5).Format("2006-1-02"), app.Invalid},
					// check_in_date is equal check_out_date
					{today.AddDate(0, 2, 0).Format(app.TimeLayout), today.AddDate(0, 2, 0).Format(app.TimeLayout), app.OutOfRange},
					// check_in_date is after check_out_date
					{today.AddDate(0, 2, 0).Format(app.TimeLayout), today.AddDate(0, 2, -1).Format(app.TimeLayout), app.OutOfRange},
					// check_in_date and check_out_date are passed
					{today.AddDate(-2, 0, 0).Format(app.TimeLayout), today.AddDate(-2, 0, 5).Format(app.TimeLayout), app.OutOfRange},
				}

				for _, c := range cases {
					str := fmt.Sprintf(`{
					    "params": {
					        "check_in_date": "%v",
					        "check_out_date": "%v",
					        "adult_count": 1,
					        "room_count": 1,
					        "partner_destination_ids": ["`+destCountry+`", "`+destCity+`"]
					    },
					    "package": {
					    	"supplier_hotel_id": "WSMA0511000113",
					        "supplier_sell_rate": {
					            "currency": "USD",
					            "value": 95.00
					        },
					        "supplier_details": {
					          "rooms": "[{\"Seq\":1,\"AdultNum\":1,\"ChildNum\":1,\"ChildAge1\":8,\"ChildAge2\":0,\"RoomPrice\":60,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"COA\"},{\"Seq\":2,\"AdultNum\":1,\"ChildNum\":0,\"ChildAge1\":0,\"ChildAge2\":0,\"RoomPrice\":35,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"N\"}]",
					          "flag_avail": "True",
				              "internal_code": "`+icode+`",
					          "cancel_policy_id": "`+app.EmptyString+`",
					          "room_category_code": "WSMA05110034",
					          "night_rates": "[95.00]"
					        }
					    },
					    "credentials": {
					    	"partner_id": "`+partnerId+`",
					        "username": "`+login+`",
					        "password": "`+password+`", 
					        "url": "`+urlCP+`",
							"pax_passport": "`+paxPassport+`"
					    }        
					}`, c.checkInDate, c.checkOutDate)
					req, _ := http.NewRequest("POST", "/additional_info", bytes.NewBufferString(str))
					w := httptest.NewRecorder()

					err := app.CancelPolicy(env, w, req)

					Expect(err.(app.HandlerError).Message).To(Equal("Validation failure"))
					Expect(err.(app.HandlerError).StatusCode).To(Equal(422))
					Expect(err.(app.HandlerError).Details).To(ConsistOf(
						app.ErrorDetail{Field: "check_in_date", Code: c.code},
						app.ErrorDetail{Field: "check_out_date", Code: c.code},
					))
				}
			})
		})

		Context("Case VII: checkIn <-> checkOut duration more then 30 days", func() {
			It("should not pass validation", func() {
				cases := []struct {
					checkInDate, checkOutDate string
				}{
					{today.AddDate(0, 2, 0).Format(app.TimeLayout), today.AddDate(0, 2, 31).Format(app.TimeLayout)},
				}
				for _, c := range cases {
					str := fmt.Sprintf(`{
					    "params": {
					        "check_in_date": "%v",
					        "check_out_date": "%v",
					        "adult_count": 1,
					        "room_count": 1,
					        "partner_destination_ids": ["`+destCountry+`", "`+destCity+`"]
					    },
					    "package": {
					    	"supplier_hotel_id": "WSMA0511000113",
					        "supplier_sell_rate": {
					            "currency": "USD",
					            "value": 95.00
					        },
					        "supplier_details": {
					          "rooms": "[{\"Seq\":1,\"AdultNum\":1,\"ChildNum\":1,\"ChildAge1\":8,\"ChildAge2\":0,\"RoomPrice\":60,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"COA\"},{\"Seq\":2,\"AdultNum\":1,\"ChildNum\":0,\"ChildAge1\":0,\"ChildAge2\":0,\"RoomPrice\":35,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"N\"}]",
					          "flag_avail": "True",
				              "internal_code": "`+icode+`",
					          "cancel_policy_id": "`+app.EmptyString+`",
					          "room_category_code": "WSMA05110034",
					          "night_rates": "[95.00]"
					        }
					    },
					    "credentials": {
					    	"partner_id": "`+partnerId+`",
					        "username": "`+login+`",
					        "password": "`+password+`", 
					        "url": "`+urlCP+`",
							"pax_passport": "`+paxPassport+`"
					    }        
					}`, c.checkInDate, c.checkOutDate)
					req, _ := http.NewRequest("POST", "/additional_info", bytes.NewBufferString(str))
					w := httptest.NewRecorder()

					err := app.CancelPolicy(env, w, req)

					Expect(err.(app.HandlerError).Message).To(Equal("Validation failure"))
					Expect(err.(app.HandlerError).StatusCode).To(Equal(422))
					Expect(err.(app.HandlerError).Details).To(ConsistOf(
						app.ErrorDetail{Field: "check_in_date", Code: app.OutOfRange},
						app.ErrorDetail{Field: "check_out_date", Code: app.OutOfRange},
					))
				}
			})
		})
	})

	Describe("API Cancellation Policy cassette. Check for valid cp request", func() {
		var vcr *app.Client

		BeforeEach(func() {
			vcr = app.NewClient("cassettes")
		})

		Context("General", func() {
			It("should return success", func() {
				vcr.UseCassette("search-then-cp-request", app.Once, func() {
					chInDate := time.Now().AddDate(0, 2, 10).Format(app.TimeLayout)
					chOutDate := time.Now().AddDate(0, 2, 15).Format(app.TimeLayout)
					sReq := &data.Request{
						Params: data.Params{
							CheckInDate:    chInDate,
							CheckOutDate:   chOutDate,
							RoomCount:      2,
							AdultCount:     3,
							Children:       []int{1, 4},
							PartnerDestIDs: []string{destCountry, destCity},
						},
						Credentials: *MockCredentials(url),
					}
					sRes, err := app.Search(vcr, sReq, app.EmptyString, app.EmptyString)
					firstHotel := func(sRes *data.Response) []data.RoomsPackage {
						for _, h := range sRes.Packages {
							return h
						}
						return []data.RoomsPackage{}
					}
					fh := firstHotel(sRes)
					cpReq := &data.CpRequest{
						Params: data.Params{
							CheckInDate:    chInDate,
							CheckOutDate:   chOutDate,
							RoomCount:      2,                               // not used, required
							AdultCount:     3,                               // not used, required
							Children:       []int{1, 4},                     // not used, required
							PartnerDestIDs: []string{destCountry, destCity}, // not used, required
						},
						Package: data.Package{
							HotelID:  string(fh[0].HotelID),
							SellRate: fh[0].SellRate,
							Details:  fh[0].Details,
						},
						Credentials: *MockCredentials(urlCP),
					}

					res, err := app.GetAddInfo(vcr, cpReq)

					Expect(err).To(BeNil())
					if len(res.CPolicy.CPBands) > 1 {
						// first, second and last items to compare time
						cp0, cp1, cpLast := res.CPolicy.CPBands[0], res.CPolicy.CPBands[1], res.CPolicy.CPBands[len(res.CPolicy.CPBands)-1]
						// check 1 second gap between dateTo cp0 and dateFrom cp1
						Expect(cp1.DateFrom.Sub(cp0.DateTo)).To(Equal(1 * time.Second))
						Expect(cp0.DateFrom).NotTo(BeNil())
						Expect(cp0.DateTo).NotTo(BeNil())
						check, _ := app.DateToRFC3339(chInDate)
						Expect(cpLast.DateTo).To(BeTemporally("==", check))
						Expect(cp0.PenaltyPercentage).NotTo(BeNil())
					}
				})
			})
		})
	})
})
