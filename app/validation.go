/*
 * validation.go contains Validation methods
 * for incoming requests
 */

package app

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"github.com/astaxie/beego/validation"

	"bitbucket.org/papazz/travflex/data"
)

type reqErr struct {
	err []ErrorDetail
}

// TimeLayout is a time format using in requests
const TimeLayout = "2006-01-02"

// ValidateSearch handler for validation search request
func ValidateSearch(r data.Request) error {
	v := &reqErr{}
	// Params
	validatorParams := validation.Validation{}
	validParams, err := validatorParams.Valid(r.Params)
	if err != nil {
		return NewHandlerError(http.StatusBadRequest, err.Error(), nil)
	}
	// Credentials
	validatorCreds := validation.Validation{}
	validCreds, err := validatorCreds.Valid(r.Credentials)
	if err != nil {
		return NewHandlerError(http.StatusBadRequest, err.Error(), nil)
	}

	if !validParams || !validCreds {
		for _, vErr := range validatorParams.Errors {
			jsonTag := getJSONStructTag(vErr.Field, r.Params, "json")
			v.err = append(v.err, ErrorDetail{Field: jsonTag, Code: getErrorCode(vErr.Name)})
		}

		for _, vErr := range validatorCreds.Errors {
			jsonTag := getJSONStructTag(vErr.Field, r.Credentials, "json")
			v.err = append(v.err, ErrorDetail{Field: jsonTag, Code: getErrorCode(vErr.Name)})
		}

		return NewHandlerError(422, "Validation failure", v.err)
	}
	// Custom validation
	v.dates(r.Params.CheckInDate, r.Params.CheckOutDate)
	v.rooms(r.Params.RoomCount, r.Params.AdultCount)
	v.adults(r.Params.AdultCount, r.Params.RoomCount)
	v.childs(r.Params.Children, r.Params.RoomCount)

	if len(v.err) > 0 {
		return NewHandlerError(422, "Validation failure", v.err)
	}

	return nil
}

// ValidateCp handler for validation cancel policy request
func ValidateCp(r data.CpRequest) error {
	v := &reqErr{}
	// Params
	validatorParams := validation.Validation{}
	validParams, err := validatorParams.Valid(r.Params)
	if err != nil {
		return NewHandlerError(http.StatusBadRequest, err.Error(), nil)
	}

	// Credentials
	validatorCreds := validation.Validation{}
	validCreds, err := validatorCreds.Valid(r.Credentials)
	if err != nil {
		return NewHandlerError(http.StatusBadRequest, err.Error(), nil)
	}

	// Package
	validatorPkg := validation.Validation{}
	validPkg, err := validatorPkg.Valid(r.Package)
	if err != nil {
		return NewHandlerError(http.StatusBadRequest, err.Error(), nil)
	}

	// Sell rate
	validatorSR := validation.Validation{}
	validSR, err := validatorSR.Valid(r.Package.SellRate)
	if err != nil {
		return NewHandlerError(http.StatusBadRequest, err.Error(), nil)
	}

	if !validParams || !validCreds || !validPkg || !validSR {
		for _, vErr := range validatorParams.Errors {
			jsonTag := getJSONStructTag(vErr.Field, r.Params, "json")
			v.err = append(v.err, ErrorDetail{Field: jsonTag, Code: getErrorCode(vErr.Name)})
		}

		for _, vErr := range validatorCreds.Errors {
			jsonTag := getJSONStructTag(vErr.Field, r.Credentials, "json")
			v.err = append(v.err, ErrorDetail{Field: jsonTag, Code: getErrorCode(vErr.Name)})
		}

		for _, vErr := range validatorPkg.Errors {
			jsonTag := getJSONStructTag(vErr.Field, r.Package, "json")
			v.err = append(v.err, ErrorDetail{Field: jsonTag, Code: getErrorCode(vErr.Name)})
		}

		for _, vErr := range validatorSR.Errors {
			jsonTag := getJSONStructTag(vErr.Field, r.Package.SellRate, "json")
			v.err = append(v.err, ErrorDetail{Field: jsonTag, Code: getErrorCode(vErr.Name)})
		}

		return NewHandlerError(422, "Validation failure", v.err)
	}
	// Custom validation
	v.dates(r.Params.CheckInDate, r.Params.CheckOutDate)
	v.details(r.Package.Details, "cancel")

	if len(v.err) > 0 {
		return NewHandlerError(422, "Validation failure", v.err)
	}

	return nil
}

// ValidateBooking handler for validation booking request
func ValidateBooking(r data.BRequest) error {
	v := &reqErr{}
	// Params
	validatorParams := validation.Validation{}
	validParams, err := validatorParams.Valid(r.Params)
	if err != nil {
		return NewHandlerError(http.StatusBadRequest, err.Error(), nil)
	}
	// Guest
	validatorGuest := validation.Validation{}
	validGuest, err := validatorGuest.Valid(r.Guest)
	if err != nil {
		return NewHandlerError(http.StatusBadRequest, err.Error(), nil)
	}
	// Package
	validatorPkg := validation.Validation{}
	validPkg, err := validatorPkg.Valid(r.Package)
	if err != nil {
		return NewHandlerError(http.StatusBadRequest, err.Error(), nil)
	}
	// Credentials
	validatorCreds := validation.Validation{}
	validCreds, err := validatorCreds.Valid(r.Credentials)
	if err != nil {
		return NewHandlerError(http.StatusBadRequest, err.Error(), nil)
	}

	if !validParams || !validPkg || !validCreds || !validGuest {
		for _, vErr := range validatorParams.Errors {
			jsonTag := getJSONStructTag(vErr.Field, r.Params, "json")
			v.err = append(v.err, ErrorDetail{Field: jsonTag, Code: getErrorCode(vErr.Name)})
		}

		for _, vErr := range validatorGuest.Errors {
			jsonTag := getJSONStructTag(vErr.Field, r.Guest, "json")
			v.err = append(v.err, ErrorDetail{Field: jsonTag, Code: getErrorCode(vErr.Name)})
		}

		for _, vErr := range validatorPkg.Errors {
			jsonTag := getJSONStructTag(vErr.Field, r.Package, "json")
			v.err = append(v.err, ErrorDetail{Field: jsonTag, Code: getErrorCode(vErr.Name)})
		}

		for _, vErr := range validatorCreds.Errors {
			jsonTag := getJSONStructTag(vErr.Field, r.Credentials, "json")
			v.err = append(v.err, ErrorDetail{Field: jsonTag, Code: getErrorCode(vErr.Name)})
		}

		return NewHandlerError(422, "Validation failure", v.err)
	}
	// Custom validation
	v.dates(r.Params.CheckInDate, r.Params.CheckOutDate)
	v.rooms(r.Params.RoomCount, r.Params.AdultCount)
	v.adults(r.Params.AdultCount, r.Params.RoomCount)
	v.childs(r.Params.Children, r.Params.RoomCount)
	v.details(r.Package.Details, "booking")

	if len(v.err) > 0 {
		return NewHandlerError(422, "Validation failure", v.err)
	}

	return nil
}

// ValidateCr handler for cancel reservation request
func ValidateCr(r data.CrRequest) error {
	v := &reqErr{}
	// Params
	if r.Params[data.CResNo] == EmptyString {
		v.err = append(v.err, ErrorDetail{Field: data.CResNo, Code: Missing})
	}
	if r.Params[data.BHotelID] == EmptyString {
		v.err = append(v.err, ErrorDetail{Field: data.BHotelID, Code: Missing})
	}
	// Credentials
	validatorCreds := validation.Validation{}
	validCreds, err := validatorCreds.Valid(r.Credentials)
	if err != nil {
		return NewHandlerError(http.StatusBadRequest, err.Error(), nil)
	}

	if !validCreds {

		for _, vErr := range validatorCreds.Errors {
			jsonTag := getJSONStructTag(vErr.Field, r.Credentials, "json")
			v.err = append(v.err, ErrorDetail{Field: jsonTag, Code: getErrorCode(vErr.Name)})
		}

		return NewHandlerError(422, "Validation failure", v.err)
	}

	if len(v.err) > 0 {
		return NewHandlerError(422, "Validation failure", v.err)
	}

	return nil
}

func (v *reqErr) dates(checkInDateStr, checkOutDateStr string) {
	if checkInDateStr == EmptyString || checkOutDateStr == EmptyString {
		return
	}

	checkInDate, errCheckInDate := time.Parse(TimeLayout, checkInDateStr)
	if errCheckInDate != nil {
		v.err = append(v.err, ErrorDetail{Field: "check_in_date", Code: Invalid})
	} else if beforeToday(checkInDate) {
		v.err = append(v.err, ErrorDetail{Field: "check_in_date", Code: OutOfRange})
	}

	checkOutDate, errCheckOutDate := time.Parse(TimeLayout, checkOutDateStr)
	if errCheckOutDate != nil {
		v.err = append(v.err, ErrorDetail{Field: "check_out_date", Code: Invalid})
	} else if beforeToday(checkOutDate) {
		v.err = append(v.err, ErrorDetail{Field: "check_out_date", Code: OutOfRange})
	}

	if errCheckInDate == nil && errCheckOutDate == nil {
		if !checkOutDate.After(checkInDate) {
			v.err = append(v.err, ErrorDetail{Field: "check_in_date", Code: OutOfRange})
			v.err = append(v.err, ErrorDetail{Field: "check_out_date", Code: OutOfRange})
		} else if checkOutDate.Sub(checkInDate) > 30*24*time.Hour {
			// Travflex: The Maximum Duration permitted is 30 nights
			v.err = append(v.err, ErrorDetail{Field: "check_in_date", Code: OutOfRange})
			v.err = append(v.err, ErrorDetail{Field: "check_out_date", Code: OutOfRange})
		}
	}
}

func (v *reqErr) rooms(rCnt, aCnt int) {
	// Max rooms = 10
	if rCnt < 1 || rCnt > 10 || rCnt/aCnt > 1 || (rCnt/aCnt == 1 && rCnt%aCnt > 0) {
		v.err = append(v.err, ErrorDetail{Field: "room_count", Code: OutOfRange})
	}
}

func (v *reqErr) adults(aCnt, rCnt int) {
	// Max adults per room = 4
	if aCnt > 4*rCnt {
		v.err = append(v.err, ErrorDetail{Field: "adult_count", Code: OutOfRange})
	}
}

func (v *reqErr) childs(children []int, rCnt int) {
	// Max children per room = 2
	cCnt := len(children)

	if cCnt < 0 || cCnt > 2*rCnt {
		v.err = append(v.err, ErrorDetail{Field: "children", Code: OutOfRange})
	}

	for _, age := range children {
		// children age range from 1 to 18
		if age < 1 || age > 18 {
			v.err = append(v.err, ErrorDetail{Field: "children", Code: Invalid})
			break
		}
	}
}

func (v *reqErr) details(d map[string]string, flag string) {
	var rooms data.RoomRate
	dec := json.NewDecoder(strings.NewReader(d[data.KRooms]))

	err := dec.Decode(&rooms.RoomSeq)
	if err != nil {
		v.err = append(v.err, ErrorDetail{Field: "supplier_details", Code: Invalid})
	}
	if d[data.KICode] == EmptyString {
		v.err = append(v.err, ErrorDetail{Field: data.KICode, Code: Missing})
	}

	if d[data.KFAvail] == EmptyString {
		v.err = append(v.err, ErrorDetail{Field: data.KFAvail, Code: Missing})
	}

	if len(d[data.KRates]) == 0 {
		v.err = append(v.err, ErrorDetail{Field: data.KRates, Code: Missing})
	}
	// validate additional fields when booking request
	if flag == "booking" {
		if d[data.KRooms] == EmptyString {
			v.err = append(v.err, ErrorDetail{Field: data.KRooms, Code: Missing})
		}

		if d[data.KCPolicyID] == EmptyString {
			v.err = append(v.err, ErrorDetail{Field: data.KCPolicyID, Code: Missing})
		}

		if d[data.KCatID] == EmptyString {
			v.err = append(v.err, ErrorDetail{Field: data.KCatID, Code: Missing})
		}
	}
}

func beforeToday(d time.Time) bool {
	t, _ := time.Parse(TimeLayout, time.Now().Format(TimeLayout))

	return d.Before(t)
}
