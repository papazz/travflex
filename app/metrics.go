package app

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
)

// Instrument is a metrics wrapper
func Instrument(name string, handler http.Handler) http.Handler {
	return prometheus.InstrumentHandler(name, handler)
}
