package app_test

import (
	"net/http"
	"testing"
	"time"

	"bitbucket.org/papazz/travflex/app"
	"bitbucket.org/papazz/travflex/data"
	"github.com/unrolled/render"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var partnerId = "ZMGH"
var login = "mghz010"
var password = "mghz010"
var icode = "CL004"
var baseUrl = "http://xml.travflex.com/11WS_SP2_1/ServicePHP/"
var url = baseUrl + "SearchHotel.php"
var urlCP = baseUrl + "ViewCancelPolicy.php"
var urlBk = baseUrl + "BookHotel.php"

var urlCr = baseUrl + "CancelRsvn.php"
var paxPassport = "MA05110059"
var destCountry = "MA05110001"
var destCity = "MA05110041"
var hotel = []string{"WSMA0511000127"}

func TestApp(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "App Suite")
}

var env *app.Env = &app.Env{
	Render: render.New(),
	HTTPClient: &http.Client{
		Timeout: app.GetTimeout(),
	},
}

var today = time.Now()

// |    Code    | Name							|
// |------------+-------------------------------|
// | MA05110184 | Australia						|
// | MA05110053 | Japan							|
// | MA05110067 | Malaysia						|
// | MA05110069 | Singapore						|
// | MA05110170 | United States of America		|
// | MA07020006 | United Kingdom				|
// | MA05110052 | South Korea					|
// | MA05110049 | China							|
// | MA05110212 | Hong Kong						|

// MockSearchParams creates new search request parameters
func MockSearchParams() *data.Request {
	return &data.Request{
		Params: data.Params{
			CheckInDate:    time.Now().AddDate(0, 1, 2).Format(app.TimeLayout),
			CheckOutDate:   time.Now().AddDate(0, 1, 3).Format(app.TimeLayout),
			RoomCount:      2,
			AdultCount:     3,
			Children:       []int{4, 5},
			PartnerDestIDs: []string{destCountry, destCity},
		},
		Credentials: *MockCredentials(url),
	}
}

// MockCredentials creates new credentials
func MockCredentials(url string) *data.Credentials {
	return &data.Credentials{
		PaxPass:   paxPassport,
		PartnerID: partnerId,
		Login:     login,
		Pass:      password,
		URL:       url,
	}
}

// GetPackageBySellRate picking up packege by price
func GetPackageBySellRate(resp *data.Response, total float64) *data.RoomsPackage {
	for _, pkgs := range resp.Packages {
		for _, pkg := range pkgs {
			if pkg.SellRate.Value == total {
				return &pkg
			}
		}
	}

	return nil
}
