package app_test

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"time"

	"bitbucket.org/papazz/travflex/app"
	"bitbucket.org/papazz/travflex/data"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Book ", func() {
	Describe("API Booking test", func() {
		Context("Case I: request is an invalid json", func() {
			It("should fail validation", func() {
				req, _ := http.NewRequest("POST", "/book", bytes.NewBufferString(`{"a":}`))
				w := httptest.NewRecorder()

				err := app.SearchHotel(env, w, req)

				Expect(err.(app.HandlerError).Message).To(Equal(`Invalid JSON was received by the server. An error occurred on the server while parsing the JSON text.`))
				Expect(err.(app.HandlerError).StatusCode).To(Equal(http.StatusBadRequest))
			})
		})

		Context("Case II: mandatory fields test", func() {
			It("should not pass validation", func() {
				req, _ := http.NewRequest("POST", "/book", bytes.NewBufferString(`{
				    "params": {
				        "check_in_date": "`+today.AddDate(0, 2, 0).Format(app.TimeLayout)+`",
				        "check_out_date": "`+today.AddDate(0, 2, 1).Format(app.TimeLayout)+`",
				        "room_count": 1,
				        "adult_count": 2,
				        "partner_destination_ids": ["`+destCountry+`", "`+destCity+`"]
				    },
				    "guest": {
				        "first_name": "Luke",
				        "last_name": "Skywalker",
				        "salutation": "Jedi."
				    },
				    "package": {
				        "supplier_sell_rate": {
				          "currency": "USD",
				          "value": 64.00
				        },
				        "supplier_details": {
				           "rooms": "[{\"Seq\":1,\"AdultNum\":2,\"ChildNum\":0,\"ChildAge1\":0,\"ChildAge2\":0,\"RoomPrice\":32,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"N\"},{\"Seq\":2,\"AdultNum\":2,\"ChildNum\":0,\"ChildAge1\":0,\"ChildAge2\":0,\"RoomPrice\":32,\"MinstayPrice\":0,\"CompulsoryPrice\":0,\"SupplementPrice\":0,\"CommissionPrice\":0,\"EarlyBirdDiscount\":0,\"PromotionBFPrice\":0,\"sRoomType\":\"N\"}]",
				           "flag_avail": "True",
				           "internal_code": "`+icode+`",
				           "room_category_code": "WSMA09040029",
				           "room_category_name": "SUPREME"
				        },
				        "is_bundled_rate": false
				    }
				}`))

				w := httptest.NewRecorder()

				err := app.BookHotel(env, w, req)
				Expect(err.(app.HandlerError).Message).To(Equal(`Validation failure`))
				Expect(err.(app.HandlerError).Details).To(ConsistOf(
					app.ErrorDetail{Field: "pax_passport", Code: app.Missing},
					app.ErrorDetail{Field: "partner_id", Code: app.Missing},
					app.ErrorDetail{Field: "username", Code: app.Missing},
					app.ErrorDetail{Field: "password", Code: app.Missing},
					app.ErrorDetail{Field: "url", Code: app.Missing},
					app.ErrorDetail{Field: "supplier_hotel_id", Code: app.Missing},
				))
				Expect(err.(app.HandlerError).StatusCode).To(Equal(422))
			})
		})
	})
	Describe("Booking API cassette. Check for valid booking request", func() {
		var vcr *app.Client

		BeforeEach(func() {
			vcr = app.NewClient("cassettes")
		})

		Context("General", func() {
			It("should return success", func() {
				vcr.UseCassette("search-book", app.Once, func() {
					chInDate := time.Now().AddDate(0, 3, 7).Format(app.TimeLayout)
					chOutDate := time.Now().AddDate(0, 3, 8).Format(app.TimeLayout)
					roomCount := 2
					adultCount := 4
					children := []int{7, 8}
					sReq := &data.Request{
						Params: data.Params{
							CheckInDate:    chInDate,
							CheckOutDate:   chOutDate,
							RoomCount:      roomCount,
							AdultCount:     adultCount,
							Children:       children,
							PartnerDestIDs: []string{destCountry, destCity},
						},
						Credentials: *MockCredentials(url),
					}
					sRes, err := app.Search(vcr, sReq, app.EmptyString, app.EmptyString)
					Expect(err).To(BeNil())

					pkg := GetPackageBySellRate(sRes, 354.82)
					bReq := &data.BRequest{
						Params: data.Params{
							CheckInDate:    chInDate,
							CheckOutDate:   chOutDate,
							RoomCount:      roomCount,
							AdultCount:     adultCount,
							Children:       children,
							PartnerDestIDs: []string{destCountry, destCity},
						},
						Guest: data.Guest{
							FirstName:  "Luke",
							LastName:   "Skywalker",
							Salutation: "Jedi.",
						},
						Package: data.Package{
							Details: map[string]string{
								data.KRooms:     pkg.Details[data.KRooms],
								data.KCatName:   pkg.Details[data.KCatName],
								data.KCatID:     pkg.Details[data.KCatID],
								data.KFood:      pkg.Details[data.KFood],
								data.KFAvail:    pkg.Details[data.KFAvail],
								data.KICode:     pkg.Details[data.KICode],
								data.KCPolicyID: pkg.Details[data.KCPolicyID],
							},
							HotelID:  string(pkg.HotelID),
							SellRate: pkg.SellRate,
						},
						Credentials: *MockCredentials(urlBk),
					}
					// extra search request for checking price
					var sResCheck *data.Response
					vcr.UseCassette("check-search-book", app.Once, func() {
						sResCheck, err = app.Search(vcr, sReq, pkg.Details[data.KCatID], string(pkg.HotelID))
					})
					Expect(err).To(BeNil())
					// booking price
					bookPrice := pkg.SellRate.Value
					// comparing prices
					Expect(app.IsPriceCorrect(sResCheck, bookPrice, bReq.Package.Details[data.KCatID], bReq.Package.Details[data.KFood])).To(Equal(true), "Search-book. Booking did not finished. No suitable price.")
					var bookRes *data.BResponse
					vcr.UseCassette("check-search-book-2", app.Once, func() {
						bookRes, err = app.Book(vcr, bReq)
					})
					Expect(err).To(BeNil())
					Expect(bookRes.Status.Code).To(Or(Equal(data.BookingStatusConfirmed), Equal(data.BookingStatusFailed)))
					Expect(bookRes.Details[data.CResNo]).NotTo(BeNil())
					Expect(bookRes.Details[data.TradeReference]).NotTo(BeNil())
					Expect(bookRes.Details[data.TotalPrice]).NotTo(BeNil())
					Expect(bookRes.Details[data.TotalCommission]).NotTo(BeNil())
				})
			})
		})

		Context("UnitTest. Check price before booking", func() {
			It("should return success", func() {
				vcr.UseCassette("search-book-price-test", app.Once, func() {
					chInDate := time.Now().AddDate(0, 2, 1).Format(app.TimeLayout)
					chOutDate := time.Now().AddDate(0, 2, 2).Format(app.TimeLayout)
					roomCount := 2
					adultCount := 4
					children := []int{7, 8}
					sReq := &data.Request{
						Params: data.Params{
							CheckInDate:    chInDate,
							CheckOutDate:   chOutDate,
							RoomCount:      roomCount,
							AdultCount:     adultCount,
							Children:       children,
							PartnerDestIDs: []string{destCountry, destCity},
						},
						Credentials: *MockCredentials(url),
					}
					sRes, _ := app.Search(vcr, sReq, app.EmptyString, app.EmptyString)
					pkg := GetPackageBySellRate(sRes, 70.00)
					bookPrice := pkg.SellRate.Value - pkg.SellRate.Value*0.0201
					Expect(app.IsPriceCorrect(sRes, bookPrice, pkg.Details[data.KCatID], pkg.Details[data.KFood])).To(Equal(false), "Supplier has been changed prices.")
				})
			})
		})
	})
})
