package app

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"strings"
	"sync"
)

const (
	// Literals intended for record modes

	// None for cause an error to be raised for any new requests
	None int = iota

	// Once for record new interaction once
	Once

	// All for never replay previously recorded interactions
	All

	// New for always record new interactions
	New
)

// ErrRecordNone message
var ErrRecordNone = errors.New("VCR: new http requests when record mode is none")

// Client used in recording request
type Client struct {
	cassetteDir     string
	cassetteName    string
	reqCount        int
	lastRequest     *http.Request
	lastRequestBody []byte
	mode            int

	Client *http.Client
	mutex  sync.Mutex
}

// Record type
type Record struct {
	Status     string
	StatusCode int
	Body       string
	Header     http.Header
	Request    Request
}

// Request type
type Request struct {
	Header http.Header
	Body   string
}

// NewClient is a constructor for Client
func NewClient(cassetteDir string) *Client {
	return &Client{
		cassetteDir: cassetteDir,
		reqCount:    Once,
		Client:      http.DefaultClient,
		mutex:       sync.Mutex{},
	}
}

// Do method for call
func (c *Client) Do(req *http.Request) (res *http.Response, err error) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	fName := cassetteFileName(c.cassetteName, c.reqCount)
	c.reqCount++

	c.lastRequest = req
	c.lastRequestBody, err = getReqBody(req)
	if err != nil {
		return nil, err
	}

	var respRecord *Record
	if c.mode != All {
		var err error
		respRecord, err = getRecord(path.Join(c.cassetteDir, fName))
		if err == nil && c.mode != New {
			return replayResponse(respRecord)
		}
	}

	if c.mode == None {
		return nil, ErrRecordNone
	}

	res, err = c.Client.Do(req)
	if err != nil {
		return nil, err
	}

	if fName == "" {
		return nil, err
	}

	if respRecord != nil && c.mode == Once {
		return
	}

	res, err = c.recordResponse(res, path.Join(c.cassetteDir, fName))
	if err != nil {
		log.Println(err)
	}

	if respRecord != nil && c.mode == New {
		return replayResponse(respRecord)
	}

	return
}

func getReqBody(r *http.Request) ([]byte, error) {
	if r.Body == nil {
		return nil, nil
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	r.Body = ioutil.NopCloser(bytes.NewBuffer(body))
	return body, nil
}

// UseCassette is the entrance in the test cases
func (c *Client) UseCassette(cn string, mode int, f func()) {
	c.mode = mode
	c.cassetteName = cn
	c.reqCount = 0
	f()
	c.resetCassette()
}

func (c *Client) resetCassette() {
	c.cassetteName = ""
	c.reqCount = 0
}

func cassetteFileName(c string, cnt int) string {
	if c == "" {
		return ""
	}

	if cnt != 0 {
		c = fmt.Sprintf("%v-%v", c, cnt)
	}
	if !strings.HasSuffix(c, ".json") {
		c += ".json"
	}

	return c
}

func (c *Client) recordResponse(res *http.Response, fileName string) (*http.Response, error) {
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	res.Body = ioutil.NopCloser(bytes.NewBuffer(body))
	res.Request.Body = ioutil.NopCloser(bytes.NewBuffer(c.lastRequestBody))

	record := Record{
		Status:     res.Status,
		StatusCode: res.StatusCode,
		Header:     res.Header,
		Body:       string(body),
		Request: Request{
			Header: res.Request.Header,
			Body:   string(c.lastRequestBody),
		},
	}

	jsonBytes, err := json.MarshalIndent(record, "", "\t")
	if err != nil {
		return nil, errors.New("Failed to encode response")
	}

	if c.cassetteDir != "" {
		err := os.MkdirAll(c.cassetteDir, 0755)
		if err != nil {
			return nil, errors.New("Failed to create cassette directory")
		}
	}

	f, err := os.Create(fileName)
	if err != nil {
		return nil, errors.New("Failed to record response")
	}

	f.Write(jsonBytes)
	return res, nil
}

func getRecord(fn string) (*Record, error) {
	f, err := os.Open(fn)
	if err != nil {
		return nil, err
	}

	file := bufio.NewReader(f)
	dec := json.NewDecoder(file)
	var record Record
	err = dec.Decode(&record)
	if err != nil {
		return nil, err
	}

	return &record, nil
}

func replayResponse(req *Record) (res *http.Response, err error) {
	preprocessed := []byte(req.Body)

	res = &http.Response{
		Status:     req.Status,
		StatusCode: req.StatusCode,
		Header:     req.Header,
		Body:       ioutil.NopCloser(bytes.NewBuffer(preprocessed)),
		Request: &http.Request{
			Header: req.Request.Header,
			Body:   ioutil.NopCloser(strings.NewReader(req.Request.Body)),
		},
	}

	return res, err
}
