package app

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"

	"bitbucket.org/papazz/travflex/data"
)

// SearchHotel is a search hotel handler
func SearchHotel(env *Env, w http.ResponseWriter, r *http.Request) error {
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		return NewHandlerError(http.StatusBadRequest, err.Error(), nil)
	}

	req := data.Request{}
	if err := json.Unmarshal(body, &req); err != nil {
		return NewHandlerError(http.StatusBadRequest, InvalidRequest, nil)
	}

	if err := ValidateSearch(req); err != nil {
		return err
	}

	res, err := Search(env.HTTPLogger, &req, EmptyString, EmptyString)
	if err != nil {
		return NewHandlerError(http.StatusInternalServerError, err.Error(), nil)
	}

	env.Render.JSON(w, http.StatusOK, res)

	return nil
}

// Search is a intermadiate function also used in tests
func Search(hc HTTPClient, req *data.Request, scode, hid string) (*data.Response, error) {
	b := buildRequest(req, scode, hid)

	res, err := searchCall(hc, b, &req.Credentials)
	if err != nil {
		return nil, err
	}
	r, err := processResult(res)
	if err != nil {
		return nil, err
	}

	return r, nil
}

func searchCall(hc HTTPClient, r *data.RequestXML, c *data.Credentials) (*data.ResponseXML, error) {
	body, _ := httpCall(hc, r, c)

	var res data.ResponseXML
	if err := xml.Unmarshal(body, &res); err != nil {
		return nil, err
	}
	if res.SHR.Error != nil {
		return nil, NewHandlerError(http.StatusOK, res.SHR.Error.ErrorDescription, nil)
	}
	return &res, nil
}

func processResult(res *data.ResponseXML) (*data.Response, error) {
	packages := make(map[data.HID][]data.RoomsPackage, 0)
	for _, hotel := range res.SHR.Hotels {
		id := data.HID(fmt.Sprintf("%v", hotel.HotelID))
		pkgs := buildPackages(id, &hotel)
		packages[id] = pkgs
	}

	return &data.Response{Packages: packages}, nil
}

func buildPackages(id data.HID, h *data.Hotel) []data.RoomsPackage {
	var packages []data.RoomsPackage
	for _, rc := range h.RoomCateg {
		packages = append(packages, *buildPackage(h, &rc, id))
	}

	return packages
}

func buildPackage(h *data.Hotel, c *data.RoomCateg, id data.HID) *data.RoomsPackage {
	var rates []data.Rate
	var totalPrice float64
	var typeName string

	for _, rt := range c.RoomType {
		for i := 0; i < 1; i++ {
			if rt.Rate[i].RoomRateInfo.Minstay.MSDay == 0 {
				for ix := range rt.Rate[i].RoomRate.RoomSeq {
					rt.Rate[i].RoomRate.RoomSeq[ix].RoomType = rt.TypeName
				}
				rates = append(rates, rt.Rate[i])
				totalPrice += rt.TotalPrice
				typeName = concatString(typeName, rt.TypeName)
			}
		}
	}

	return &data.RoomsPackage{
		Partner:  PARTNER,
		Supplier: SUPPLIER,
		SellRate: data.CurrencyValue{
			Currency: h.Currency,
			Value:    roundFloat(totalPrice),
		},
		HotelID: id,
		RoomDetails: &data.RoomDetails{
			Description: typeName,
			Food:        foodCode(c.BFType),
			RoomType:    typeName,
		},
		Details: buildDetails(c, rates, h),
	}
}

func buildDetails(c *data.RoomCateg, rates []data.Rate, h *data.Hotel) map[string]string {
	var seq []data.RoomSeq
	var rooms, nRates []byte
	var nPrices []float64

	for _, rs := range rates[0].RoomRate.RoomSeq {
		seq = append(seq, rs)
	}
	rooms, _ = json.Marshal(seq)

	for _, rate := range rates {
		nPrices = append(nPrices, rate.NightPrice)
	}
	nRates, _ = json.Marshal(nPrices)

	return map[string]string{
		data.KRooms:     string(rooms),
		data.KICode:     h.InternalCode,
		data.KCatName:   c.Name,
		data.KCatID:     c.Code,
		data.KFAvail:    h.Avail,
		data.KCPolicyID: h.CancelPolicyID,
		data.KFood:      c.BFType,
		data.KRates:     string(nRates),
	}
}

func buildRequest(req *data.Request, scode, hid string) *data.RequestXML {
	sReq := &data.RequestXML{
		Agent: data.Agent{
			AgentID:   req.Credentials.PartnerID,
			LoginName: req.Credentials.Login,
			Password:  req.Credentials.Pass,
		},
		SHR: data.SHReq{
			PaxPassport: req.Credentials.PaxPass,
			DestCountry: req.Params.PartnerDestIDs[0],
			DestCity:    req.Params.PartnerDestIDs[1],
			ServiceCode: scode,
			Period: data.Period{
				CheckIn:  req.Params.CheckInDate,
				CheckOut: req.Params.CheckOutDate,
			},
			RoomInfo:  buildRoomsInfo(req),
			FlagAvail: FLAGAVAIL,
			HotelID: data.HotelID{
				Value: hid,
			},
		},
	}

	return sReq
}

func buildRoomsInfo(req *data.Request) []data.RoomInfo {
	var s settlers = make([]set, req.Params.RoomCount)
	s.adults(req)
	s.childs(req)

	roomsInfo := make([]data.RoomInfo, len(s))
	for i, s := range s {
		roomsInfo[i] = data.RoomInfo{
			AdultNum: data.AdultNum{
				Number:     s.adults,
				RQBedChild: data.RQBedChildDefault,
			},
			ChildAges: buildChildAges(s.childs),
		}
	}

	return roomsInfo
}

func buildChildAges(childs []int) data.ChildAges {
	childAges := data.ChildAges{}
	childAges.ChildAgeList = make([]data.ChildAge, len(childs))
	for i := 0; i < len(childs); i++ {
		childAges.ChildAgeList[i] = data.ChildAge{Age: childs[i]}
	}

	return childAges
}
