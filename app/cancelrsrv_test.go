package app_test

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"time"

	"bitbucket.org/papazz/travflex/app"
	"bitbucket.org/papazz/travflex/data"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Cancel Reservation ", func() {
	Describe("API CR test", func() {
		Context("Case I: request is an invalid json", func() {
			It("should fail validation", func() {
				req, _ := http.NewRequest("POST", "/cancel", bytes.NewBufferString(`{"a":}`))
				w := httptest.NewRecorder()

				err := app.SearchHotel(env, w, req)

				Expect(err.(app.HandlerError).Message).To(Equal(`Invalid JSON was received by the server. An error occurred on the server while parsing the JSON text.`))
				Expect(err.(app.HandlerError).StatusCode).To(Equal(http.StatusBadRequest))
			})
		})

		Context("Case II: mandatory fields test", func() {
			It("should not pass validation", func() {
				req, _ := http.NewRequest("POST", "/cancel", bytes.NewBufferString(`{
				    "additional_references": {
				    },
				    "credentials": {
				    	"partner_id": "`+partnerId+`",
				        "username": "`+login+`",
				        "password": "`+password+`",
				        "url": "`+url+`",
				        "pax_passport": "`+paxPassport+`"
				    }
				}
				`))

				w := httptest.NewRecorder()

				err := app.CancelReserv(env, w, req)
				Expect(err.(app.HandlerError).Message).To(Equal(`Validation failure`))
				Expect(err.(app.HandlerError).Details).To(ConsistOf(
					app.ErrorDetail{Field: data.CResNo, Code: app.Missing},
					app.ErrorDetail{Field: data.BHotelID, Code: app.Missing},
				))
				Expect(err.(app.HandlerError).StatusCode).To(Equal(422))
			})
		})
	})
	Describe("Cancel Reservation API cassette. Check for valid request", func() {
		var vcr *app.Client

		BeforeEach(func() {
			vcr = app.NewClient("cassettes")
		})

		Context("General", func() {
			It("should return success", func() {
				vcr.UseCassette("search-book-cancel", app.Once, func() {
					chInDate := time.Now().AddDate(0, 4, 1).Format(app.TimeLayout)
					chOutDate := time.Now().AddDate(0, 4, 2).Format(app.TimeLayout)
					roomCount := 2
					adultCount := 4
					children := []int{3, 6}
					sReq := &data.Request{
						Params: data.Params{
							CheckInDate:    chInDate,
							CheckOutDate:   chOutDate,
							RoomCount:      roomCount,
							AdultCount:     adultCount,
							Children:       children,
							PartnerDestIDs: []string{destCountry, destCity},
						},
						Credentials: *MockCredentials(url),
					}
					sRes, err := app.Search(vcr, sReq, app.EmptyString, app.EmptyString)
					Expect(err).To(BeNil())

					pkg := GetPackageBySellRate(sRes, 84.00)

					bReq := &data.BRequest{
						Params: data.Params{
							CheckInDate:    chInDate,
							CheckOutDate:   chOutDate,
							RoomCount:      roomCount,
							AdultCount:     adultCount,
							Children:       children,
							PartnerDestIDs: []string{destCountry, destCity},
						},
						Guest: data.Guest{
							FirstName:  "Luke",
							LastName:   "Skywalker",
							Salutation: "Jedi.",
						},
						Package: data.Package{
							Details: map[string]string{
								data.KRooms:     pkg.Details[data.KRooms],
								data.KCatName:   pkg.Details[data.KCatName],
								data.KCatID:     pkg.Details[data.KCatID],
								data.KFood:      pkg.Details[data.KFood],
								data.KFAvail:    pkg.Details[data.KFAvail],
								data.KICode:     pkg.Details[data.KICode],
								data.KCPolicyID: pkg.Details[data.KCPolicyID],
							},
							HotelID:  string(pkg.HotelID),
							SellRate: pkg.SellRate,
						},
						Credentials: *MockCredentials(urlBk),
					}
					// extra search request for checking price
					var sResCheck *data.Response
					vcr.UseCassette("search-check-book-cancel", app.Once, func() {
						sResCheck, err = app.Search(vcr, sReq, pkg.Details[data.KCatID], string(pkg.HotelID))
					})
					Expect(err).To(BeNil())
					// booking price
					bookPrice := pkg.SellRate.Value
					// comparing prices
					Expect(app.IsPriceCorrect(sResCheck, bookPrice, bReq.Package.Details[data.KCatID], bReq.Package.Details[data.KFood])).To(Equal(true), "Search-book-cancel. Cancel reservation did not finished. No suitable price.")
					var bookRes *data.BResponse
					vcr.UseCassette("search-check-book-cancel-2", app.Once, func() {
						bookRes, err = app.Book(vcr, bReq)
					})
					Expect(err).To(BeNil())
					Expect(bookRes.Status.Code).To(Equal(data.BookingStatusConfirmed))

					crReq := &data.CrRequest{
						Params: map[string]string{
							data.CResNo:   bookRes.Details[data.TradeReference],
							data.BHotelID: bookRes.ConfDetails.CustomerReference,
						},
						Credentials: *MockCredentials(urlCr),
					}
					var cancelRes *data.CrResponse
					vcr.UseCassette("search-check-book-cancel-3", app.Once, func() {
						cancelRes, err = app.CancelRsrv(vcr, crReq)
					})
					Expect(err).To(BeNil())
					Expect(cancelRes.Details).NotTo(BeNil())
				})
			})
		})
	})
})
