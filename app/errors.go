package app

const (
	// Missing literal set when element is absent
	Missing ErrorCode = "missing"

	// Invalid literal set when element is wrong type
	Invalid ErrorCode = "invalid"

	// OutOfRange literal set when element values
	// is out of allowable bondaries
	OutOfRange ErrorCode = "out_of_range"
)

// InvalidRequest is a Message for incorrect request JSON API
const InvalidRequest = "Invalid JSON was received by the server. " +
	"An error occurred on the server while parsing the JSON text."

// ErrorCode used for error message
type ErrorCode string

// ErrorDetail type intended for expand error message
type ErrorDetail struct {
	Field string    `json:"field"`
	Code  ErrorCode `json:"code"`
}

// HandlerError is a main error type
type HandlerError struct {
	StatusCode int           `json:"-"`
	Message    string        `json:"message"`
	Details    []ErrorDetail `json:"errors,omitempty"`
	Request    interface{}   `json:"-"`
}

func (e HandlerError) Error() string {
	return e.Message
}

// NewHandlerError error handler constructor
func NewHandlerError(status int, message string, details []ErrorDetail) HandlerError {
	return HandlerError{
		StatusCode: status,
		Message:    message,
		Details:    details,
	}
}

func getErrorCode(name string) ErrorCode {
	switch name {
	case "Required":
		return Missing
	case "Range":
		return OutOfRange
	case "Min":
		return OutOfRange
	}

	return Missing
}
