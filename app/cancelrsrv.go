package app

import (
	"encoding/json"
	"encoding/xml"
	"io/ioutil"
	"net/http"

	"bitbucket.org/papazz/travflex/data"
)

// CancelReserv is a cancel reservation handler
func CancelReserv(env *Env, w http.ResponseWriter, r *http.Request) error {
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		return NewHandlerError(http.StatusBadRequest, err.Error(), nil)
	}

	req := data.CrRequest{}
	if err := json.Unmarshal(body, &req); err != nil {
		return NewHandlerError(http.StatusBadRequest, InvalidRequest, nil)
	}

	if err := ValidateCr(req); err != nil {
		return err
	}

	res, err := CancelRsrv(env.HTTPLogger, &req)
	if err != nil {
		return NewHandlerError(http.StatusInternalServerError, err.Error(), nil)
	}

	env.Render.JSON(w, http.StatusOK, res)

	return nil
}

// CancelRsrv is an intermediate function
func CancelRsrv(httpClient HTTPClient, r *data.CrRequest) (*data.CrResponse, error) {
	req := buildCrRequest(r)

	crRes, err := crCall(httpClient, req, &r.Credentials)
	if err != nil {
		return nil, err
	}

	return processCrResult(crRes, r), nil
}

func crCall(httpClient HTTPClient, crReq *data.CrRequestXML, c *data.Credentials) (*data.CrResponseXML, error) {
	body, err := httpCall(httpClient, crReq, c)
	if err != nil {
		return nil, err
	}

	var crRes data.CrResponseXML

	err = xml.Unmarshal(body, &crRes)
	if err != nil {
		return nil, err
	}

	if crRes.CRR.Error != nil {
		return nil, NewHandlerError(http.StatusBadRequest, crRes.CRR.Error.Description, nil)
	}

	return &crRes, nil
}

func buildCrRequest(req *data.CrRequest) *data.CrRequestXML {
	return &data.CrRequestXML{
		Agent: data.Agent{
			AgentID:   req.Credentials.PartnerID,
			LoginName: req.Credentials.Login,
			Password:  req.Credentials.Pass,
		},
		CRsvn: data.CancelReservation{
			ResNo:    req.Params[data.CResNo],
			BHotelID: req.Params[data.BHotelID],
		},
	}
}

func processCrResult(res *data.CrResponseXML, req *data.CrRequest) *data.CrResponse {
	return &data.CrResponse{
		Penalty: &data.CurrencyValue{
			Currency: res.CRR.Policies.ChargePrice.Currency,
			Value:    res.CRR.Policies.ChargePrice.Price,
		},
		Details: map[string]string{
			"sdBookingReference":      res.CRR.HBooking,
			"sdCancellationReference": res.CRR.CancelHBooking,
			"sdResNo":                 res.CRR.ResNo,
			"sdOSRefNo":               res.CRR.OSRefNo,
		},
	}
}
