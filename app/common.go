package app

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"bitbucket.org/papazz/travflex/data"
)

type set struct {
	adults int
	childs []int
}

type settlers []set

func httpCall(httpClient HTTPClient, request interface{}, c *data.Credentials) ([]byte, error) {
	xmlReq, _ := xml.Marshal(request)

	params := url.Values{}
	params.Set("requestXML", FilterXMLRequestBody(xmlReq))
	req, err := http.NewRequest("POST", c.URL, strings.NewReader(params.Encode()))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Accept", "application/xml")

	resp, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

func (s settlers) adults(req *data.Request) {
	numOfAdultsPerRoom := req.Params.AdultCount / req.Params.RoomCount
	remain := req.Params.AdultCount % req.Params.RoomCount

	for i := 0; i < remain; i++ {
		s[i].adults = numOfAdultsPerRoom + 1
	}

	for i := remain; i < req.Params.RoomCount; i++ {
		s[i].adults = numOfAdultsPerRoom
	}
}

func (s settlers) childs(req *data.Request) {
	if len(req.Params.Children) > 0 {
		chlist := make([]int, len(req.Params.Children))
		copy(chlist, req.Params.Children)
		numOfChildsPerRoom := len(chlist) / req.Params.RoomCount
		remain := len(chlist) % req.Params.RoomCount
		// settle who remains
		for i := 0; i < remain; i++ {
			for j := 0; j < numOfChildsPerRoom+1; j++ {
				s.settle(i, &chlist)
			}
		}
		// settle by number per room
		for i := remain; i < req.Params.RoomCount; i++ {
			for j := 0; j < numOfChildsPerRoom; j++ {
				s.settle(i, &chlist)
			}
		}
	}
}

func (s settlers) settle(idx int, chlist *[]int) {
	s[idx].childs = append(s[idx].childs, (*chlist)[0])
	(*chlist)[0] = (*chlist)[len(*chlist)-1]
	(*chlist) = (*chlist)[:len(*chlist)-1]
}

func buildChildList(chNum, chAge1, chAge2 int) []int {
	switch chNum {
	case 1:
		return []int{chAge1}
	case 2:
		return []int{chAge1, chAge2}
	default:
		return []int{}
	}
}

func decRooms(d string) *data.RoomRate {
	var rate data.RoomRate
	dec := json.NewDecoder(strings.NewReader(d))
	dec.Decode(&rate.RoomSeq)
	return &rate
}

func roundFloat(x float64) (res float64) {
	res, _ = strconv.ParseFloat(fmt.Sprintf("%.2f", x), 64)
	return res
}

func foodCode(bftype string) data.FoodCode {
	switch bftype {
	case "ABBF":
		return data.BreakfastBasis
	case "ABF":
		return data.BreakfastBasis
	case "ABFCB":
		return data.BreakfastBasis
	case "ALI":
		return data.AllInclusiveBasis
	case "ALIS":
		return data.AllInclusiveBasis
	case "ASBF":
		return data.BreakfastBasis
	case "BABF":
		return data.BreakfastBasis
	case "BAHBBF":
		return data.BreakfastBasis
	case "BB":
		return data.BreakfastBasis
	case "BBD":
		return data.HalfBoardBasis
	case "BBF":
		return data.BreakfastBasis
	case "BBR":
		return data.BreakfastBasis
	case "BBX":
		return data.BreakfastBasis
	case "BD":
		return data.DinnerBasis
	case "BFCP":
		return data.DinnerBasis
	case "BFPROMO":
		return data.DinnerBasis
	case "BFSH":
		return data.BreakfastBasis
	case "BFSTRM":
		return data.BreakfastBasis
	case "BRB":
		return data.BreakfastBasis
	case "BRU":
		return data.BreakfastBasis
	case "CBBF":
		return data.BreakfastBasis
	case "CBF":
		return data.BreakfastBasis
	case "CHB":
		return data.BreakfastBasis
	case "CNBF":
		return data.BreakfastBasis
	case "COLD":
		return data.BreakfastBasis
	case "COLDBF":
		return data.BreakfastBasis
	case "DINEAALL":
		return data.AllInclusiveBasis
	case "EBF":
		return data.BreakfastBasis
	case "ENBF":
		return data.BreakfastBasis
	case "EXB":
		return data.BreakfastBasis
	case "FB":
		return data.FullBoardBasis
	case "FBALL":
		return data.AllInclusiveBasis
	case "FUB":
		return data.FullBoardBasis
	case "FULLBBFD":
		return data.BreakfastBasis
	case "FULLBF":
		return data.BreakfastBasis
	case "HB":
		return data.HalfBoardBasis
	case "HBD":
		return data.HalfBoardBasis
	case "HBL":
		return data.HalfBoardBasis
	case "HOTBF":
		return data.BreakfastBasis
	case "IBF":
		return data.BreakfastBasis
	case "IRBF":
		return data.BreakfastBasis
	case "ISR":
		return data.BreakfastBasis
	case "ITA":
		return data.BreakfastBasis
	case "JPA":
		return data.BreakfastBasis
	case "NYB":
		return data.BreakfastBasis
	case "OBF":
		return data.BreakfastBasis
	case "RD":
		return data.DinnerBasis
	case "RL":
		return data.LunchBasis
	case "RO":
		return data.RoomOnly
	case "ROPROMO":
		return data.RoomOnly
	case "SC":
		return data.RoomOnly
	case "SBF":
		return data.BreakfastBasis
	case "SCBF":
		return data.BreakfastBasis
	case "SETBF":
		return data.BreakfastBasis
	case "SHBF":
		return data.BreakfastBasis
	case "SHT":
		return data.BreakfastBasis
	case "SHTBF":
		return data.BreakfastBasis
	case "THBF":
		return data.BreakfastBasis
	case "VBB":
		return data.BreakfastBasis
	case "VI":
		return data.RoomOnly
	default:
		return data.RoomOnly
	}
}
