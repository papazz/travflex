package app

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/papazz/travflex/data"
)

// BookHotel is a Booking Hotel handler
func BookHotel(env *Env, w http.ResponseWriter, r *http.Request) error {
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		return NewHandlerError(http.StatusBadRequest, err.Error(), nil)
	}

	req := data.BRequest{}
	if err := json.Unmarshal(body, &req); err != nil {
		return NewHandlerError(http.StatusBadRequest, InvalidRequest, nil)
	}

	if err := ValidateBooking(req); err != nil {
		return err
	}

	// Check price using search request with ServiceCode
	// before booking
	sReq := data.Request{
		Params:      req.Params,
		Credentials: req.Credentials,
	}
	sRes, err := Search(env.HTTPLogger, &sReq, req.Package.Details[data.KCatID], req.Package.HotelID)

	if !IsPriceCorrect(sRes, req.Package.SellRate.Value, req.Package.Details[data.KCatID], req.Package.Details[data.KFood]) {
		return NewHandlerError(http.StatusNotFound, err.Error(), nil)
	}

	res, err := Book(env.HTTPLogger, &req)
	if err != nil {
		return NewHandlerError(http.StatusInternalServerError, err.Error(), nil)
	}

	env.Render.JSON(w, http.StatusOK, res)

	return nil
}

// Book intermediate function
func Book(hc HTTPClient, req *data.BRequest) (*data.BResponse, error) {
	b := buildBRequest(req)
	res, err := bookCall(hc, b, &req.Credentials)
	if err != nil {
		return nil, err
	}
	r := processBResult(res)

	return r, nil
}

func buildBRequest(req *data.BRequest) *data.BRequestXML {
	sReq := &data.BRequestXML{
		Agent: data.Agent{
			AgentID:   req.Credentials.PartnerID,
			LoginName: req.Credentials.Login,
			Password:  req.Credentials.Pass,
		},
		BookHotel: data.BookHotel{
			FinishBook:  "Y",
			OSRefNo:     generateOSRN(),
			PaxPassport: req.Credentials.PaxPass,
			HLists:      buildHotelList(req),
		},
	}

	return sReq
}

func bookCall(hc HTTPClient, r *data.BRequestXML, c *data.Credentials) (*data.BResponseXML, error) {
	body, _ := httpCall(hc, r, c)

	var res data.BResponseXML
	if err := xml.Unmarshal(body, &res); err != nil {
		return nil, err
	}

	if res.BHR.Error != nil {
		return nil, NewHandlerError(http.StatusOK, res.BHR.Error.ErrorDescription, nil)
	}
	return &res, nil
}

func processBResult(res *data.BResponseXML) *data.BResponse {
	var totalPrice, commPrice float64
	for _, v := range res.BHR.CompleteService.HBookIDs[0].RoomCatgInfo.RoomCatg.Room.SeqRoom {
		totalPrice += v.PriceInfo.TotalPrice
		commPrice += v.PriceInfo.CommPrice
	}
	return &data.BResponse{
		Status: data.BStatusType{
			Code: resolveStatus(res),
		},
		Details: map[string]string{
			data.BookingReference: res.BHR.CompleteService.HBookIDs[0].ID,
			data.TradeReference:   res.BHR.ResNo.FileNum,
			data.TotalPrice:       fmt.Sprintf("%.2f", totalPrice),
			data.TotalCommission:  fmt.Sprintf("%.2f", totalPrice),
		},
		ConfDetails: data.ConfirmationDetails{
			CustomerReference: res.BHR.CompleteService.HBookIDs[0].ID,
		},
	}
}

func resolveStatus(res *data.BResponseXML) data.BStatusCode {
	if res.BHR.UnCompleteService.Desc != EmptyString {
		return data.BookingStatusFailed
	}
	return data.BookingStatusConfirmed
}

func buildHotelList(req *data.BRequest) []data.HotelList {
	var list = make([]data.HotelList, 0)
	list = append(list, data.HotelList{
		Seq:          "1",
		InternalCode: req.Package.Details[data.KICode],
		FlagAvail:    req.Package.Details[data.KFAvail],
		HotelID:      req.Package.HotelID,
		RoomCatg:     *buildRoomCatg(req),
	})

	return list
}

func buildRoomCatg(req *data.BRequest) *data.RoomCatg {

	return &data.RoomCatg{
		BFType:   req.Package.Details[data.KFood],
		CheckIn:  req.Params.CheckInDate,
		CheckOut: req.Params.CheckOutDate,
		CatgName: req.Package.Details[data.KCatName],
		CatgID:   req.Package.Details[data.KCatID],
		RoomType: buildRoomType(req),
	}
}

func buildRoomType(req *data.BRequest) []data.BRoomType {
	var list = make([]data.BRoomType, 0)
	for _, rs := range decRooms(req.Package.Details[data.KRooms]).RoomSeq {
		list = append(list, data.BRoomType{
			Seq:            rs.Seq,
			TypeName:       rs.RoomType,
			Price:          rs.RoomPrice,
			RQBedChild:     data.RQBedChildDefault,
			AdultNum:       rs.AdultNum,
			ChildAges:      buildChildAges(buildChildList(rs.ChildNum, rs.ChildAge1, rs.ChildAge2)),
			PaxInformation: *buildPaxInformation(req, rs.AdultNum+rs.ChildNum),
		})
	}
	return list
}

func buildPaxInformation(req *data.BRequest, an int) *data.PaxInformation {
	name := buildGuestName(req.Guest)
	p := &data.PaxInformation{}
	p.PaxInfo = make([]data.PaxInfo, an)
	for i := range p.PaxInfo {
		p.PaxInfo[i].ID = i + 1
		p.PaxInfo[i].Name = name
	}
	return p
}

func buildGuestName(guest data.Guest) string {
	fn := strings.TrimSpace(guest.FirstName)
	ln := strings.TrimSpace(guest.LastName)

	return fmt.Sprintf("%s %s, %s", ln, fn, chooseSalutation(guest.Salutation))
}

func generateOSRN() string {
	rand.Seed(time.Now().UnixNano())
	b := make([]rune, OSRefNoLength)
	l := []rune(LETTERS)
	for i := range b {
		b[i] = l[rand.Intn(len(l))]
	}
	return string(b)
}

// IsPriceCorrect check price correctness before booking
func IsPriceCorrect(sRes *data.Response, bPrice float64, catID, food string) bool {
	for _, pkgs := range sRes.Packages {
		// here is loop because supplier can return multiple categories
		// with the same Room Categ Code
		for _, pkg := range pkgs {
			if pkg.Details[data.KCatID] == catID && pkg.Details[data.KFood] == food {
				if bPrice >= pkg.SellRate.Value || bPrice+bPrice*0.02 >= pkg.SellRate.Value {
					return true
				}
			}
		}
	}
	return false
}

func chooseSalutation(s string) string {
	for _, ds := range data.GuestSalutation {
		if ds == strings.TrimSpace(s) {
			return s
		}
	}
	return data.GuestSalutation[0]
}
