package app

import "net/http"

// HTTPClient interface
type HTTPClient interface {
	Do(req *http.Request) (resp *http.Response, err error)
}

// ClientFunc satisfies Client interface
type ClientFunc func(*http.Request) (*http.Response, error)

// Do is the worker
func (f ClientFunc) Do(r *http.Request) (*http.Response, error) {
	return f(r)
}

// ClientWrapper is the client wrapper
type ClientWrapper func(HTTPClient) HTTPClient
