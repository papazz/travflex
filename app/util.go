package app

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"
)

const (
	// PARTNER literal used in response
	PARTNER = "travflex"

	// SUPPLIER literal used in response
	SUPPLIER = "travflex"

	// OSRefNoLength is a length used for generating key
	OSRefNoLength = 10

	// LETTERS alphabet source for key generating
	LETTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

	// FLAGAVAIL availability request apartment key
	FLAGAVAIL = "Y"

	//NonRefExCancelDays 999 and more days refer doc
	NonRefExCancelDays = 999

	//FullCharge 100%
	FullCharge = 100

	//EmptyString literal
	EmptyString = ""
)

// FilterXMLRequestBody utility function intended
// to resolve symbols in XML request
func FilterXMLRequestBody(xmlReqBody []byte) string {
	xmlReqStr := string(xmlReqBody)
	problematicCharacters := map[string]string{
		"&#39;":     "'",
		"&amp;#39;": "'",
		"&#xA;":     " ",
		"&amp;#xA;": " ",
		"&#x9;":     "    ",
		"&amp;#x9;": "    ",
	}

	for problematic, substitute := range problematicCharacters {
		xmlReqStr = strings.Replace(xmlReqStr, problematic, substitute, -1)
	}

	return xmlReqStr
}

// GetTimeout used to limit request time to supplier
func GetTimeout() time.Duration {
	defaultTimeout := time.Duration(40 * time.Second)

	timeoutStr := os.Getenv("HTTP_CLIENT_TIMEOUT")
	if timeoutStr == EmptyString {
		return defaultTimeout
	}

	timeout, err := strconv.ParseInt(timeoutStr, 10, 64)
	if err != nil {
		return defaultTimeout
	}

	return time.Duration(timeout)
}

// DateToRFC3339 cast to RFC3339
func DateToRFC3339(date string) (time.Time, error) {
	res, err := time.Parse(time.RFC3339, date+"T00:00:00+14:00")
	if err != nil {
		return time.Time{}, err
	}

	return res.UTC(), nil
}

func concatString(head, tail string) string {
	if head == EmptyString {
		return tail
	}

	return head + ", " + tail
}

// ToWorstCaseTimezone converts timestamp to worst case timezone and return it in UTC time
func ToWorstCaseTimezone(timestamp string) (time.Time, error) {
	if len(timestamp) < 19 {
		return time.Time{}, fmt.Errorf("invalid timestamp, timestamp should be at least 19 chars, timestamp: %v", timestamp)
	}

	// UTC +14 is the first time zone to start a new day (worst case timezone)
	// https://en.wikipedia.org/wiki/UTC%2B14:00
	dateStr := timestamp[0:19] + "+14:00"
	result, err := time.Parse(time.RFC3339, dateStr)

	if err != nil {
		return result, fmt.Errorf("invalid timestamp, timestamp doesnt match the format, timestamp, %v, err: %v ", timestamp, err)
	}

	return result.UTC(), nil
}

func removeDuplicateString(pslice *[]string) {
	seen := make(map[string]bool)
	j := 0
	for i, el := range *pslice {
		if !seen[el] {
			seen[el] = true
			(*pslice)[j] = (*pslice)[i]
			j++
		}
	}
	*pslice = (*pslice)[:j]
}
