package app

import (
	"log"
	"net/http"
	"reflect"

	"github.com/unrolled/render"
)

// Env contains Render and HTTP Client
type Env struct {
	Render     *render.Render
	HTTPClient HTTPClient
	HTTPLogger HTTPClient
}

// Handler overrided with Environment
type Handler struct {
	E *Env
	H func(e *Env, w http.ResponseWriter, r *http.Request) error
}

func (h Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := h.H(h.E, w, r)
	if err != nil {
		switch err := err.(type) {
		case HandlerError:
			log.Printf("handler error status code: %v, message: %v, details: %v, request: %+v\n", err.StatusCode, err.Message, err.Details, err.Request)

			h.E.Render.JSON(w, err.StatusCode, err)
		default:
			h.E.Render.JSON(w, http.StatusInternalServerError, err)
		}
	}
}

func getJSONStructTag(fieldName string, s interface{}, tagName string) string {
	t := reflect.TypeOf(s)
	field, found := t.FieldByName(fieldName)

	if !found {
		log.Println("failed to find the field by the name of", fieldName)
		return fieldName
	}

	return field.Tag.Get(tagName)
}
