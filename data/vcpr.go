package data

// VCPRReq s a view cancellation policy request to supplier
type VCPRReq struct {
	PaxPassport  string `xml:"PaxPassport" valid:"Required"`
	HotelID      HID    `xml:"HotelId" valid:"Required"`
	InternalCode string `xml:"InternalCode"`
	CheckInDate  string `xml:"dtCheckIn" valid:"Required"`
	CheckOutDate string `xml:"dtCheckOut" valid:"Required"`
	RoomsInfo    []CpRoomInfo
	FlagAvail    string `xml:"flagAvail" valid:"Required"`
	CpID         string `xml:"CancelPolicyID" valid:"Required"`
}

// VCPRRes is a view cancellation policy response from supplier
type VCPRRes struct {
	HotelID   string   `xml:"HotelId"`
	HotelName string   `xml:"HotelName"`
	Policies  Policies `xml:"Policies"`
}
