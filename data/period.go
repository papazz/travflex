package data

// Period of settlement
type Period struct {
	CheckIn  string `xml:"checkIn,attr" valid:"Required"`
	CheckOut string `xml:"checkOut,attr" valid:"Required"`
}
