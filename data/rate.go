package data

// Rate price detail
type Rate struct {
	NightPrice   float64      `xml:"NightPrice,attr"`
	OffSet       string       `xml:"OffSet,attr"`
	RoomRate     RoomRate     `xml:"RoomRate"`
	RoomRateInfo RoomRateInfo `xml:"RoomRateInfo"`
}
