package data

// ChildAges refers to the children in room
type ChildAges struct {
	ChildAgeList []ChildAge `xml:"ChildAge"`
}

// ChildAge children age
type ChildAge struct {
	Age int `xml:",chardata"`
}
