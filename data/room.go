package data

import "encoding/xml"

const (
	// RQBedChildDefault default value for request bed child
	RQBedChildDefault = "N"
)

// RoomInfo room information
type RoomInfo struct {
	XMLName   xml.Name  `xml:"RoomInfo" valid:"Required"`
	AdultNum  AdultNum  `xml:"AdultNum" json:"adult_num" valid:"Required"`
	ChildAges ChildAges `xml:"ChildAges,omitempty" json:"child_ages,omitempty"`
}

// RoomCateg room category detail
type RoomCateg struct {
	XMLName    xml.Name `xml:"RoomCateg"`
	Code       string   `xml:"Code,attr"`
	Name       string   `xml:"Name,attr"`
	NetPrice   string   `xml:"NetPrice,attr"`
	GrossPrice string   `xml:"GrossPrice,attr"`
	CommPrice  string   `xml:"CommPrice,attr"`
	Price      string   `xml:"Price,attr"`
	BFType     string   `xml:"BFType,attr"`
	RoomType   []RoomType
}

// RoomCatgCode type
type RoomCatgCode struct {
	BFType string `xml:"BFType,attr"`
	Name   string `xml:"Name,attr"`
	Value  string `xml:",chardata"`
}

// RoomType room type price and detail
type RoomType struct {
	XMLName       xml.Name `xml:"RoomType"`
	TypeName      string   `xml:"TypeName,attr"`
	NumRooms      string   `xml:"NumRooms,attr"`
	TotalPrice    float64  `xml:"TotalPrice,attr"`
	AvrNightPrice float64  `xml:"avrNightPrice,attr"`
	RTGrossPrice  float64  `xml:"RTGrossPrice,attr"`
	RTCommPrice   float64  `xml:"RTCommPrice,attr"`
	RTNetPrice    float64  `xml:"RTNetPrice,attr"`
	Rate          []Rate   `xml:"Rate"`
}

// BRoomType room type price and detail for booking
type BRoomType struct {
	Seq            int            `xml:"Seq,attr"`
	TypeName       string         `xml:"TypeName,attr"`
	RQBedChild     string         `xml:"RQBedChild,attr"`
	Price          float64        `xml:"Price,attr"`
	AdultNum       int            `xml:"AdultNum"`
	ChildAges      ChildAges      `xml:"ChildAges"`
	PaxInformation PaxInformation `xml:"PaxInformation"`
}

// RoomRate room price detail
type RoomRate struct {
	XMLName xml.Name  `xml:"RoomRate"`
	RoomSeq []RoomSeq `xml:"RoomSeq"`
}

// RoomSeq information
type RoomSeq struct {
	Seq               int     `xml:"No,attr" json:"Seq"`
	AdultNum          int     `xml:"AdultNum,attr" json:"AdultNum"`
	ChildNum          int     `xml:"ChildNum,attr" json:"ChildNum"`
	ChildAge1         int     `xml:"ChildAge1,attr" json:"ChildAge1"`
	ChildAge2         int     `xml:"ChildAge2,attr" json:"ChildAge2"`
	RoomPrice         float64 `xml:"RoomPrice,attr" json:"RoomPrice"`
	MinstayPrice      float64 `xml:"MinstayPrice,attr" json:"MinstayPrice"`
	CompulsoryPrice   float64 `xml:"CompulsoryPrice,attr" json:"CompulsoryPrice"`
	SupplementPrice   float64 `xml:"SupplementPrice,attr" json:"SupplementPrice"`
	CommissionPrice   float64 `xml:"CommissionPrice,attr" json:"CommissionPrice"`
	EarlyBirdDiscount float64 `xml:"EarlyBirdDiscount,attr" json:"EarlyBirdDiscount"`
	PromotionBFPrice  float64 `xml:"PromotionBFPrice,attr" json:"PromotionBFPrice"`
	SRoomType         string  `xml:"sRoomType,attr" json:"sRoomType"`
	RoomType          string  `json:"RoomType"`
}

// RoomDetails response
type RoomDetails struct {
	Food                 FoodCode              `json:"food"`
	Description          string                `json:"description"`
	NonRefundable        *bool                 `json:"non_refundable,omitempty"`
	RoomType             string                `json:"room_type"`
	RoomView             string                `json:"room_view,omitempty"`
	RoomDescriptionHints *RoomDescriptionHints `json:"room_description_hints"`
}

// RoomDescriptionHints has additional info on the room
type RoomDescriptionHints struct {
	BedHint string `json:"bed_hint"`
}

// RoomRateInfo is a room rate information
type RoomRateInfo struct {
	Minstay Minstay `xml:"Minstay"`
}

// Minstay minimum stay promotion detail
type Minstay struct {
	MSDay int `xml:"MSDay"`
}

// RoomCatg room category detail
type RoomCatg struct {
	BFType   string      `xml:"BFType,attr"`
	CheckIn  string      `xml:"checkIn,attr"`
	CheckOut string      `xml:"checkOut,attr"`
	CatgName string      `xml:"CatgName,attr"`
	CatgID   string      `xml:"CatgId,attr"`
	RoomType []BRoomType `xml:"RoomType"`
}
