package data

// Credentials part
type Credentials struct {
	PartnerID string `json:"partner_id" valid:"Required"`
	Login     string `json:"username" valid:"Required"`
	Pass      string `json:"password" valid:"Required"`
	URL       string `json:"url" valid:"Required"`
	PaxPass   string `json:"pax_passport" valid:"Required"`
}

// Agent is the supplier conn. info
type Agent struct {
	AgentID   string `xml:"AgentId" json:"agent_id" valid:"Required"`
	LoginName string `xml:"LoginName" json:"login" valid:"Required"`
	Password  string `xml:"Password" json:"password" valid:"Required"`
}
