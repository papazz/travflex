/*
 * response.go contains Incoming from Supplier (XML)
 * and Outgoing API (JSON) Response structures
 *
 */

package data

import (
	"encoding/xml"
	"sync"
)

// Incoming XML response

// ResponseXML search hotel supplier
type ResponseXML struct {
	XMLName xml.Name `xml:"Service_SearchHotel"`
	SHR     SHRes    `xml:"SearchHotel_Response"`
}

// CpResponseXML cancel policy supplier
type CpResponseXML struct {
	XMLName xml.Name `xml:"Service_ViewCancelPolicy"`
	VCPR    VCPRRes  `xml:"ViewCancelPolicy_Response"`
}

// BResponseXML booking supplier
type BResponseXML struct {
	XMLName xml.Name `xml:"BookingHotel"`
	BHR     BHR      `xml:"BookHotel_Response"`
}

// CrResponseXML cancel reservation supplier
type CrResponseXML struct {
	XMLName xml.Name `xml:"Service_CancelRSVN"`
	CRR     CRR      `xml:"CancelReservation_Response"`
}

// CRR cancel reservation response supplier
type CRR struct {
	Policies       CrPolicies `xml:"Policies"`
	CancelResult   string     `xml:"CancelResult"`
	ResNo          string     `xml:"ResNo"`
	OSRefNo        string     `xml:"OSRefNo"`
	HBooking       string     `xml:"Hbooking"`
	CancelHBooking string     `xml:"CancelHbooking"`
	Error          *CRRError  `xml:"Error,omitempty"`
}

type CRRError struct {
	Description string `xml:"ErrorDescription"`
}

// CrPolicies cancel reservation policy
type CrPolicies struct {
	ChargePrice ChargePrice `xml:"ChargePrice"`
}

// Outgoing search response

// Packages pack
type Packages map[HID][]RoomsPackage

// Response API
type Response struct {
	sync.Mutex
	Packages Packages `json:"packages"`
}

// RoomsPackage API
type RoomsPackage struct {
	Partner          string                   `json:"partner"`
	Supplier         string                   `json:"supplier"`
	SellRate         CurrencyValue            `json:"supplier_sell_rate"`
	ZumataCommission *CurrencyValue           `json:"zumata_commission,omitempty"`
	HotelID          HID                      `json:"supplier_hotel_id"`
	RoomDetails      *RoomDetails             `json:"supplier_room_details"`
	Details          map[string]string        `json:"supplier_details"`
	TaxesAndFees     map[string]CurrencyValue `json:"taxes_and_fees,omitempty"`
	IsBundledRate    bool                     `json:"is_bundled_rate"`
}

// CpResponse cancel policy API
type CpResponse struct {
	CPolicy *CancellationPolicy `json:"cancellation_policy"`
}

// BResponse booking API
type BResponse struct {
	Details     map[string]string   `json:"supplier_details"`
	ConfDetails ConfirmationDetails `json:"confirmation_details"`
	Status      BStatusType         `json:"status"`
}

// CrResponse booking API
type CrResponse struct {
	Penalty *CurrencyValue    `json:"penalty"`
	Details map[string]string `json:"supplier_details"`
}

// ResponseError API error response type
type ResponseError struct {
	ErrorDescription string `xml:"ErrorDescription"`
}
