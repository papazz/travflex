package data

import (
	"encoding/xml"
)

// Incoming requests

// Request Search hotel API
type Request struct {
	Params      Params      `json:"params" valid:"Required"`
	Credentials Credentials `json:"credentials" valid:"Required"`
}

// CpRequest cancel policy API
type CpRequest struct {
	Params      Params      `json:"params" valid:"Required"`
	Package     Package     `json:"package" valid:"Required"`
	Credentials Credentials `json:"credentials" valid:"Required"`
}

// Package used in cancellation policy and Booking
type Package struct {
	SellRate   CurrencyValue     `json:"supplier_sell_rate" valid:"Required"`
	HotelID    string            `json:"supplier_hotel_id" valid:"Required"`
	Details    map[string]string `json:"supplier_details" valid:Required"`
	BookingKey string            `json:"booking_key,omitempty"`
}

// BRequest booking API
type BRequest struct {
	Params      Params      `json:"params" valid:"Required"`
	Guest       Guest       `json:"guest" valid:"Required"`
	Package     Package     `json:"package" valid:"Required"`
	Credentials Credentials `json:"credentials" valid:"Required"`
}

// CrRequest cancel reservation API
type CrRequest struct {
	Params      map[string]string `json:"additional_references" valid:"Required"`
	Credentials Credentials       `json:"credentials" valid:"Required"`
}

// Params request parameters
type Params struct {
	CheckInDate    string   `json:"check_in_date" valid:"Required"`
	CheckOutDate   string   `json:"check_out_date" valid:"Required"`
	RoomCount      int      `json:"room_count" valid:"Required;Range(1,10)"`
	AdultCount     int      `json:"adult_count" valid:"Required;Range(1,40)"`
	Children       []int    `json:"children"`
	PartnerDestIDs []string `json:"partner_destination_ids" valid:"Required"`
}

// Outgoing requests

// RequestXML supplier search hotel XML
type RequestXML struct {
	XMLName xml.Name `xml:"Service_SearchHotel" valid:"Required"`
	Agent   Agent    `xml:"AgentLogin" valid:"Required"`
	SHR     SHReq    `xml:"SearchHotel_Request" valid:"Required"`
}

// CpRequestXML cancel policy supplier XML
type CpRequestXML struct {
	XMLName xml.Name `xml:"Service_ViewCancelPolicy" valid:"Required"`
	Agent   Agent    `xml:"AgentLogin" valid:"Required"`
	VCPR    VCPRReq  `xml:"ViewCancelPolicy_Request" valid:"Required"`
}

// BRequestXML booking supplier XML
type BRequestXML struct {
	XMLName   xml.Name  `xml:"Service_BookHotel" valid:"Required"`
	Agent     Agent     `xml:"AgentLogin" valid:"Required"`
	BookHotel BookHotel `xml:"BookHotel" valid:"Required"`
}

// CrRequestXML cancel reservation supplier XML
type CrRequestXML struct {
	XMLName xml.Name          `xml:"Service_CancelRSVN" valid:"Required"`
	Agent   Agent             `xml:"AgentLogin" valid:"Required"`
	CRsvn   CancelReservation `xml:"CancelReservation" valid:"Required"`
}

// CancelReservation nested params
type CancelReservation struct {
	ResNo    string `xml:"ResNo" valid:"Required"`
	BHotelID string `xml:"Hbooking" valid:"Required"`
}

// Guest details
type Guest struct {
	Salutation string `json:"salutation"`
	FirstName  string `json:"first_name" valid:"Required"`
	LastName   string `json:"last_name" valid:"Required"`
}
