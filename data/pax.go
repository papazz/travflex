package data

// PaxInformation passenger in room
type PaxInformation struct {
	PaxInfo []PaxInfo `xml:"PaxInfo"`
}

// PaxInfo passenger name
type PaxInfo struct {
	ID   int    `xml:"Id,attr"`
	Name string `xml:",chardata"`
}
