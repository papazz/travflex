package data

// Zumata booking status codes
const (
	BookingStatusConfirmed BStatusCode = iota + 1
	BookingStatusPending
	BookingStatusCancelled
	BookingStatusFailed
	BookingStatusUnknown

	// supplier details information
	CResNo           = "result_no"
	BHotelID         = "booking_reference"
	BookingReference = "BookingReference"
	TradeReference   = "TradeReference"
	TotalPrice       = "TotalPrice"
	TotalCommission  = "TotalCommission"

	KRooms     = "rooms"
	KICode     = "internal_code"
	KFAvail    = "flag_avail"
	KCPolicyID = "cancel_policy_id"
	KCatID     = "room_category_code"
	KCatName   = "room_category_name"
	KFood      = "food_type"
	KRates     = "night_rates"
)

var GuestSalutation = []string{"Mr.", "Ms.", "Miss", "Mrs"}

// BookHotel type
type BookHotel struct {
	OSRefNo     string      `xml:"OSRefNo"`
	PaxPassport string      `xml:"PaxPassport"`
	HLists      []HotelList `xml:"HotelList"`
	FinishBook  string      `xml:"FinishBook"`
}

// BHR hotel booking response
type BHR struct {
	ResNo             ResNo             `xml:"ResNo"`
	CompleteService   CompleteService   `xml:"CompleteService"`
	Error             *ResponseError    `xml:"Error"`
	UnCompleteService UnCompleteService `xml:"UnCompleteService"`
}

// ResNo reference with other service and also use to check booking detai
type ResNo struct {
	RPCurrency  string `xml:"RPCurrency,attr"`
	FinishBook  string `xml:"FinishBook,attr"`
	OSRefNo     string `xml:"OSRefNo,attr"`
	PaxPassport string `xml:"PaxPassport,attr"`
	Status      string `xml:"Status,attr"`
	FileNum     string `xml:",chardata"`
}

// CompleteService is a complete service detail
type CompleteService struct {
	HBookIDs []HBookID `xml:"HBookId"`
}

// UnCompleteService incomplete service
type UnCompleteService struct {
	Desc string `xml:",chardata"`
}

// HBookID hotel booking detail
type HBookID struct {
	Status       string `xml:"Status,attr"`
	InternalCode string `xml:"InternalCode,attr"`
	VoucherDt    string `xml:"VoucherDt,attr"`
	VoucherNo    string `xml:"VoucherNo,attr"`
	ID           string `xml:"Id,attr"`
	RoomCatgInfo struct {
		RoomCatg struct {
			Room struct {
				SeqRoom []BookSeqRoom `xml:"SeqRoom"`
			} `xml:"Room"`
		} `xml:"RoomCatg"`
	} `xml:"RoomCatgInfo"`
}

// BookSeqRoom booking details
type BookSeqRoom struct {
	PriceInfo BPriceInfo `xml:"PriceInformation"`
}

// BPriceInfo booking details
type BPriceInfo struct {
	TotalPrice float64 `xml:"TotalPrice"`
	CommPrice  float64 `xml:"CommissionPrice"`
}

// ConfirmationDetails response
type ConfirmationDetails struct {
	CustomerReference string `json:"customer_reference"`
}

// BStatusCode is the booking status code
type BStatusCode int

// BStatusType is the booking status
type BStatusType struct {
	Code BStatusCode `json:"code"`
}
