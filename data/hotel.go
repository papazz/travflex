package data

import "encoding/xml"

// HID Hotel ID
type HID string

// HotelID comlex type
type HotelID struct {
	XMLName      xml.Name `xml:"HotelId" valid:"Required"`
	InternalCode string   `xml:"InternalCode,attr"`
	Value        string   `xml:",chardata"`
}

// Hotel options
type Hotel struct {
	CancelPolicyID string `xml:"CancelPolicyId,attr" valid:"Required,attr"`
	Currency       string `xml:"Currency,attr" valid:"Required,attr"`
	HotelID        string `xml:"HotelId,attr" valid:"Required,attr"`
	HotelName      string `xml:"HotelName,attr" valid:"Required,attr"`
	InternalCode   string `xml:"InternalCode,attr" valid:"Required,attr"`
	MarketName     string `xml:"MarketName,attr" valid:"Required,attr"`
	Rating         string `xml:"Rating,attr" valid:"Required,attr"`
	Avail          string `xml:"avail,attr" valid:"Required,attr"`
	DtCheckIn      string `xml:"dtCheckIn,attr" valid:"Required,attr"`
	DtCheckOut     string `xml:"dtCheckOut,attr" valid:"Required,attr"`
	RoomCateg      []RoomCateg
}

// HotelList list of Hotel
type HotelList struct {
	Seq          string   `xml:"Seq,attr"`
	InternalCode string   `xml:"InternalCode,attr"`
	FlagAvail    string   `xml:"flagAvail,attr"`
	HotelID      string   `xml:"HotelId"`
	OrgHBID      string   `xml:"OrgHBId,omitempty"`
	OrgResID     string   `xml:"OrgResId,omitempty"`
	RequestDes   string   `xml:"RequestDes,omitempty"`
	RoomCatg     RoomCatg `xml:"RoomCatg"`
}
