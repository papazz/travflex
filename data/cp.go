package data

import (
	"encoding/xml"
	"time"
)

// CpRoomInfo type
type CpRoomInfo struct {
	XMLName    xml.Name  `xml:"RoomInfo" valid:"Required"`
	SeqNo      int       `xml:"SeqNo,attr"`
	RoomType   string    `xml:"RoomType,attr"`
	Adults     int       `xml:"AdultNum" valid:"Required"`
	ChildAges  ChildAges `xml:"ChildAges,omitempty"`
	RQBedChild string    `xml:"RQBedChild,attr"`
}

// Policies list
type Policies struct {
	List []Policy `xml:"Policy" valid:"Required"`
}

// Policy is the cancel policy detail
type Policy struct {
	FromDate     string       `xml:"FromDate,attr"`
	ToDate       string       `xml:"ToDate,attr"`
	RoomCatgCode RoomCatgCode `xml:"RoomCatgCode"`
	ExCancelDays int          `xml:"ExCancelDays"`
	ChargeType   string       `xml:"ChargeType"`
	ChargeRate   float64      `xml:"ChargeRate"`
	Desc         desc         `xml:"Description"`
}

type desc struct {
	Text string `xml:",chardata"`
}

// CancellationPolicy response type
type CancellationPolicy struct {
	Remarks string  `json:"remarks,omitempty"`
	CPBands CPBands `json:"cancellation_policies"`
}

// CPBands list of CPBand
type CPBands []CPBand

// CPBand is the cancellation policy band
type CPBand struct {
	PenaltyPercentage float64   `json:"penalty_percentage"`
	DateFrom          time.Time `json:"date_from"`
	DateTo            time.Time `json:"date_to"`
}

func (p Policies) Len() int { return len(p.List) }

func (p Policies) Less(i, j int) bool {
	return p.List[i].ExCancelDays < p.List[j].ExCancelDays
}

func (p Policies) Swap(i, j int) {
	p.List[i], p.List[j] = p.List[j], p.List[i]
}
