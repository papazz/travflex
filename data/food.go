package data

type FoodCode int

// Zumata food codes
const (
	RoomOnly FoodCode = iota + 1 // Starts from 1
	BreakfastBasis
	LunchBasis
	DinnerBasis
	HalfBoardBasis
	FullBoardBasis
	AllInclusiveBasis
)
