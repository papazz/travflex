package data

// AdultNum type inn XML request
type AdultNum struct {
	RQBedChild string `xml:"RQBedChild,attr"`
	RoomType   string `xml:"RoomType,attr"`
	Number     int    `xml:",chardata"`
}
