package data

// SHReq is a search hotel request type to supplier
type SHReq struct {
	PaxPassport string `xml:"PaxPassport" valid:"Required"`
	DestCountry string `xml:"DestCountry" valid:"Required"`
	DestCity    string `xml:"DestCity" valid:"Required"`
	ServiceCode string `xml:"ServiceCode,omitempty"`
	HotelID     HotelID
	Period      Period `xml:"Period"`
	RoomInfo    []RoomInfo
	FlagAvail   string `xml:"flagAvail,omitempty"`
}

// SHRes is a search hotel response type from supplier
type SHRes struct {
	Hotels []Hotel        `xml:"Hotel"`
	Error  *ResponseError `xml:"Error"`
}
