package data

// CurrencyValue type used for response
type CurrencyValue struct {
	Currency string  `json:"currency" valid:"Required"`
	Value    float64 `json:"value" valid:"Required"`
}

// ChargePrice charge price type
type ChargePrice struct {
	Price    float64 `xml:"Price,attr"`
	Currency string  `xml:"Currency,attr"`
}
